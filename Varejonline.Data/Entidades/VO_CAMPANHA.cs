﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_CAMPANHA : VO_MODIFICACOES
    {
        public VO_CAMPANHA()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public int COD_CAMPANHA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public int CANAL { get; set; }

        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string MARCA { get; set; }

        [StringLength(30, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string NOME_CAMPANHA { get; set; }

        [StringLength(100, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string DESC_CAMPANHA { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<VO_CAMPANHA_CLIENTE> VO_CAMPANHA_CLIENTEs { get; set; }
        public virtual ICollection<VO_CAMPANHA_RETORNO> VO_CAMPANHA_RETORNOs { get; set; }
    }
}
