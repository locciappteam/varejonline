﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Varejonline.Data.Entidades
{
    public class VO_CANAL
    {
        public VO_CANAL()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public int COD_CANAL { get; set; }
        
        public int CANAL_SAP { get; set; }
        
        public string CATEGORIA_CANAL { get; set; }
        
        public string DESCRICAO { get; set; }
        
        public string ABREVIACAO{ get; set; }

        public string NOME_ARQUIVO { get; set; }

        public bool TEM_CAMPANHA { get; set; }


        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

    }
}
