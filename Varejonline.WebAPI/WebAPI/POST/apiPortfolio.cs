﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Request;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiPortfolio
    {
        private static List<portifolioJson> getPortfolioJson(DateTime DataTransferencia)
        {
            List<portifolioJson> portfolio;
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                portfolio = (
                    from
                        portifolioCapa in _ctx.voPortfolio
                    select new portifolioJson
                    {
                        codigoPortfolio = portifolioCapa.COD_PORTFOLIO,
                        portifolioProdutos = (
                            from
                                portifolioProduto in _ctx.voPortfolioProduto
                            where
                                portifolioProduto.DATA_ALTERACAO >= DataTransferencia
                                && portifolioCapa.COD_PORTFOLIO == portifolioProduto.COD_PORTFOLIO
                            select new portifolioProdutosJson
                            {
                                codigoPortfolio = portifolioProduto.COD_PORTFOLIO,
                                codigoSistema = portifolioProduto.COD_PRODUTO,
                                fatorCompra = portifolioProduto.FATOR_COMPRA,
                                compraMaxima = portifolioProduto.COMPRA_MAXIMA,
                                observacao = portifolioProduto.OBSERVACAO,
                                nivelDestaque = portifolioProduto.NIVEL_DESTAQUE
                            }
                        ).ToList()
                    }
                )
                .Where(a => a.portifolioProdutos.Count > 0)
                .ToList();
            }

            return portfolio;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia)
        {
            List<portifolioJson> PortfolioJson = getPortfolioJson(DataTransferencia);

            foreach (var portfolio in PortfolioJson)
            {
                try
                {
                    string jsonRequest = JsonConvert.SerializeObject(portfolio.portifolioProdutos);
                    string responseData;

                    Task.Run(async () =>
                    {
                        responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);
                        saveResponse(portfolio.codigoPortfolio, responseData);
                    }).GetAwaiter().GetResult();
                }
                catch (Exception Ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                    continue;
                }
            }
        }

        private static void saveResponse(string codigoPortfolio, string responseData)
        {
            if (responseData != "Falho")
            {
                responseJson ReponseJson = JsonConvert.DeserializeObject<responseJson>(responseData);
                string[] DescErro = ReponseJson.errorDescription.Split('|').Skip(1).ToArray();

                List<VO_PORTFOLIO_RETORNO> VoPortfolioRetorno = new List<VO_PORTFOLIO_RETORNO>();

                int i = 0;
                foreach (var retornosuccessProcessed in ReponseJson.successProcessed)
                {
                    string CodProduto = ReponseJson.successProcessed[i].Substring(ReponseJson.successProcessed[i].IndexOf("Produto:") + 9);

                    VoPortfolioRetorno.Add(new VO_PORTFOLIO_RETORNO
                    {
                        COD_PORTFOLIO = codigoPortfolio,
                        COD_PRODUTO = CodProduto,
                        RETORNO_SUCESSO = "Retorno com sucesso!",
                        RETORNO_FALHO = null,
                        DESC_ERRO = null
                    });

                    i++;
                }

                i = 0;
                foreach (var retornofailedProcessed in ReponseJson.failedProcessed)
                {
                    VoPortfolioRetorno.Add(new VO_PORTFOLIO_RETORNO
                    {
                        COD_PORTFOLIO = codigoPortfolio,
                        COD_PRODUTO = retornofailedProcessed,
                        RETORNO_SUCESSO = null,
                        RETORNO_FALHO = "Retorno com erro!",
                        DESC_ERRO = DescErro[ReponseJson.failedProcessed.IndexOf(retornofailedProcessed)]
                    });

                    i++;
                }

                foreach (var retorno in VoPortfolioRetorno.Distinct())
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_PORTFOLIO_RETORNO>().Add(retorno);
                            _ctx.SaveChanges();
                        }
                    }
                    catch (Exception Ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO SALVAR RETORNO [{retorno.COD_PORTFOLIO}-{retorno.COD_PRODUTO}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }
    }
}
