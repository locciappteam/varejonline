﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.Data.ViewModel;

namespace Varejonline.Planilhas.Importacao
{
    public class portfolioCsv
    {
        public static void Importar(string pathArquivo)
        {
            int ErrosImportacao = 0;
            int i = 0;
            string[] arquivosPortfolio = Directory.GetFiles(pathArquivo.ToUpper(), "Portfolio*.csv".ToUpper());
             
            if (arquivosPortfolio.Any())
            {
                List<VO_CANAL> CanaisCampanhas = new DataContextVarejonline().voCanal.AsNoTracking().ToList();

                foreach (string arquivo in arquivosPortfolio)
                {
                    
                    FileInfo info = new FileInfo(pathArquivo + arquivo);

                    string nomePortfolio = info.Name.ToUpper().Replace("PORTFOLIO_","").Replace(".CSV", "");

                    DateTime InicioImportacao = DateTime.Now;
                    List<VO_IMPORTACAO_LOG> ListaLogImportacao = new List<VO_IMPORTACAO_LOG>();
                    string novoNomeArquivo = String.Format($"{Path.GetFileNameWithoutExtension(arquivo)}_{InicioImportacao.ToString("dd.MM.yyyy_hh.mm")}.csv");

                    string[] contentFile = File.ReadLines(arquivo, Encoding.GetEncoding("iso-8859-1")).ToArray();

                    var DtInicial   =  DateTime.Today;
                    var DtFinal     =  DtInicial.AddYears(50); 

                    vmVoPortfolio VmVoPortfolio = new vmVoPortfolio
                    {
                        CANAL_SAP = Convert.ToInt32(contentFile[1].Split(';')[0]),
                        CATEGORIA_CANAL = contentFile[1].Split(';')[1],
                        MARCA = contentFile[1].Split(';')[2],
                        CAMPANHA = contentFile[1].Split(';')[3],                     
                        REGIAO = contentFile[1].Split(';')[6]
                    };
 
                    //BUSCA NA TABELA CANAL QUAL O CÓDIGO DO CANAL LOCCITANE E ATRIBUI AO VmVoPortfolio 
                    var canal = CanaisCampanhas.Where(p => p.CANAL_SAP== VmVoPortfolio.CANAL_SAP && p.CATEGORIA_CANAL == VmVoPortfolio.CATEGORIA_CANAL).ToList().Single();
                    VmVoPortfolio.CANAL = canal.COD_CANAL;

                    VmVoPortfolio.DATA_INICIO = VmVoPortfolio.CANAL != 20 ? DtInicial : Convert.ToDateTime(contentFile[1].Split(';')[4]);
                    VmVoPortfolio.DATA_FINAL = VmVoPortfolio.CANAL != 20 ? DtFinal : Convert.ToDateTime(contentFile[1].Split(';')[5]);

                    // VALIDAÇÕES INICIAIS: invalidar se nova DATA DO INICIO DA NOVA CAMPANHA FOR MENOR QUE D-2
                    if (VmVoPortfolio.DATA_INICIO < DateTime.Now.AddDays(-2))
                        throw new Exception("DATA DE INICIO DA NOVA CAMPANHA NÃO PODE SER MENOR QUE A DATA ATUAL");

                    if (VmVoPortfolio.DATA_FINAL < VmVoPortfolio.DATA_INICIO)
                        throw new Exception("DATA FINAL DA NOVA NOVA NÃO PODE SER MENOR MENOR QUE SUA DATA Inicial");


                    VO_PORTFOLIO voPortfolio = new VO_PORTFOLIO
                    {
                        COD_PORTFOLIO = nomePortfolio,
                        NOME = "PORTFOLIO " + nomePortfolio.ToUpper() + " " + VmVoPortfolio.MARCA.ToUpper(),
                        MARCA = VmVoPortfolio.MARCA,
                    };

                    //ADICIONAR / EDITAR PORTFOLIO
                    incluirPortfolio(voPortfolio);

                    //EXCLUSÃO DE PORTFOLIO =======================================================
                    ErrosImportacao += Excluir(voPortfolio.COD_PORTFOLIO);

                    //IMPORTAÇÃO DE PRODUTOS NO PORTFOLIO =========================================
                    Log.Write(String.Format($"{DateTime.Now} : Adicionando Produtos no Portfolio [{voPortfolio.COD_PORTFOLIO}]"));
                    foreach (string line in contentFile.Skip(3))
                    {
                        VO_IMPORTACAO_LOG voImportacaoLog = new VO_IMPORTACAO_LOG();

                        i++;
                        VO_PORTFOLIO_PRODUTO voPortfolioProduto = new VO_PORTFOLIO_PRODUTO
                        {
                            COD_PORTFOLIO = voPortfolio.COD_PORTFOLIO.Trim(),
                            COD_PRODUTO = line.Split(';')[0].Trim(),
                            COMPRA_MAXIMA = line.Split(';')[1].Trim() == "" ? 0 : Convert.ToInt32(line.Split(';')[1]),
                            FATOR_COMPRA = line.Split(';')[2].Trim() == "" ? 1 : Convert.ToDecimal(line.Split(';')[2]),
                            OBSERVACAO = (line.Split(';')[3].Trim() == null || line.Split(';')[3].Trim() == string.Empty) ? "     " : line.Split(';')[3].Trim().Replace("'",""),
                            NIVEL_DESTAQUE = line.Split(';')[4].Trim()
                        };

                        voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                        voImportacaoLog.NOME_ARQUIVO = novoNomeArquivo;
                        voImportacaoLog.TIPO_IMPORTACAO = "PORTFOLIO";
                        voImportacaoLog.CODIGO_IDETIFICADOR = voPortfolioProduto.COD_PORTFOLIO + "@" + voPortfolioProduto.COD_PRODUTO;

                        try
                        {

                            using (DataContextVarejonline _ctx = new DataContextVarejonline())
                            {
                                voImportacaoLog.STATUS = "INSERIDO";
                                _ctx.Set<VO_PORTFOLIO_PRODUTO>().Add(voPortfolioProduto);
                                _ctx.SaveChanges();
                            }

                            ListaLogImportacao.Add(voImportacaoLog);
                        }
                        catch (Exception Ex)
                        {
                            ErrosImportacao++;
                            voImportacaoLog.STATUS = "ERRO";

                            //ESPAÇO PARA TRADUÇÃO DE ERROS //
                            string erro = Ex.Message;
                            if (erro.Contains("FK_VO_PORTFOLIO_PRODUTO_VO_PRODUTO"))
                            {
                                erro = "PRODUTO NÃO CADASTRADO";
                            }
                            // ------------------------------- //

                           voImportacaoLog.DESC_ERRO = erro;
                            ListaLogImportacao.Add(voImportacaoLog);
                            Log.Write(String.Format($"{DateTime.Now} : ERRO [LINHA: {i} | PRODUTO: {line.Split(';')[0]}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
                            continue;
                        }
                    }

                    //ATRIBUI CAMPANHAS AO CLIENTE ================================================
                   if(canal.TEM_CAMPANHA)
                   {
                        foreach (var log in AtribuirCampanha(VmVoPortfolio, nomePortfolio, novoNomeArquivo))
                        {
                            ListaLogImportacao.Add(log);
                        }
 
                   }

                    foreach (var log in ListaLogImportacao)
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_IMPORTACAO_LOG>().Add(log);
                            _ctx.SaveChanges();
                        }
                    }

                    //RELATÓRIO DE IMPORTAÇÃO DE PLANILHA 

                    List<string> VoEmailDestinatario = new DataContextVarejonline().voEmailDestinatario
                                .AsNoTracking()
                                .Where(a => a.REL_PORTFOLIO == true)
                                .Select(a => a.EMAIL)
                                .ToList();

                    Email.Enviar(
                               "am03svc-cegid@loccitane.com.br", String.Join(",", VoEmailDestinatario),
                               MensagemHtmlLogPortfolio.Formatar(ListaLogImportacao),
                               String.Format($"Relatório Importação Planilha - Portfolio {novoNomeArquivo}"),
                               true
                       );

                    //MOVE ARQUIVO PARA PASTA DE PROCESSADO =======================================
                    Log.Write(String.Format($"{DateTime.Now} :: Enviando para pasta Processado - Portfolio {arquivo}"));
                    File.Move(
                        arquivo,
                        String.Format($@"{pathArquivo}\Processado\{novoNomeArquivo}"));
                   
                    /*
                    if (ErrosImportacao > 0)
                    {
                        Email.Enviar(
                                "am03svc-cegid@loccitane.com.br",
                                "dlbr-itapplications@loccitane.com",
                                "Erro ao importar a Planilha de Portfolio. Verifique o LOG.",
                                "Erro ao importar Planilha - Portfolio"
                        );
                    }
                    */
                }
            }
            else
            {
                Log.Write(String.Format($"{DateTime.Now} :: Planilha CSV - Não localizada {pathArquivo}Portfolio*.csv"));
            }
        }
        private static int Excluir(string CodPortfolio)
        {
            int ErroImportacao = 0;

            try
            {
                Log.Write(String.Format($"{DateTime.Now} : Excluindo Produtos do Portfólio [{CodPortfolio}]"));
                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    List<VO_PORTFOLIO_PRODUTO> VoPortfolioProduto = (
                        from
                            a in _ctx.voPortfolioProduto
                        where
                            a.COD_PORTFOLIO == CodPortfolio
                        select
                            a
                    ).ToList();

                    if (VoPortfolioProduto.Any())
                    {
                        _ctx.Set<VO_PORTFOLIO_PRODUTO>().RemoveRange(VoPortfolioProduto);
                        _ctx.SaveChanges();
                    }
                }
            }
            catch (Exception Ex)
            {
                ErroImportacao++;

                Log.Write(String.Format($"{DateTime.Now} : ERRO [{CodPortfolio}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
            }

            return ErroImportacao;
        }
        private static List<VO_IMPORTACAO_LOG> AtribuirCampanha(vmVoPortfolio portfolioLoccitane, string nomePortfolio, string arquivo)
        {
            Log.Write(String.Format($"{DateTime.Now} : IMPORTANDO CAMPANHAS"));

            int ErrosImportacao = 0;
            DateTime InicioImportacao = DateTime.Now;
            List<VO_IMPORTACAO_LOG> ListaLogImportacao = new List<VO_IMPORTACAO_LOG>();

            VO_CAMPANHA_ATIVA novaCampanha = new VO_CAMPANHA_ATIVA
            {
                CANAL = portfolioLoccitane.CANAL,
                MARCA = portfolioLoccitane.MARCA,
                CAMPANHA = portfolioLoccitane.CAMPANHA,
                REGIAO = portfolioLoccitane.REGIAO.ToUpper(),
                DATA_INICIO = portfolioLoccitane.DATA_INICIO,
                DATA_FINAL = portfolioLoccitane.DATA_FINAL
            };

            VO_IMPORTACAO_LOG voImportacaoLog = new VO_IMPORTACAO_LOG();
            voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
            voImportacaoLog.NOME_ARQUIVO = arquivo;
            voImportacaoLog.TIPO_IMPORTACAO = "CAMPANHA";
            voImportacaoLog.CODIGO_IDETIFICADOR = novaCampanha.CANAL + "@" + novaCampanha.REGIAO + "@" + novaCampanha.CAMPANHA;

            //Adiciona ou Altera Campanha Ativa ===================================================
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {

                VO_CAMPANHA_ATIVA VoCampanhaAtivaAtual = _ctx.voCampanhaAtiva
                    .AsNoTracking()
                    .OrderByDescending(p=> p.DATA_FINAL)
                    .FirstOrDefault(a =>
                        a.CANAL         == novaCampanha.CANAL
                        && a.MARCA      == novaCampanha.MARCA
                        && a.REGIAO     == novaCampanha.REGIAO);

                if(VoCampanhaAtivaAtual != null)
                {
                    if (VoCampanhaAtivaAtual.CAMPANHA == novaCampanha.CAMPANHA)
                    {
                        // APENAS RENOVA A DATA DA CAMPANHA
                        VoCampanhaAtivaAtual.DATA_INICIO = novaCampanha.DATA_INICIO;
                        VoCampanhaAtivaAtual.DATA_FINAL = novaCampanha.DATA_FINAL;
                    }
                    else if (VoCampanhaAtivaAtual.DATA_FINAL >= novaCampanha.DATA_INICIO)
                    {
                        // DESSA FORMA ANTECIPA O FIM DA CAMPANHA ATIVA ATUAL 
                        VoCampanhaAtivaAtual.DATA_FINAL = novaCampanha.DATA_INICIO.AddDays(-1);

                        if (VoCampanhaAtivaAtual.DATA_INICIO >= VoCampanhaAtivaAtual.DATA_FINAL)
                            VoCampanhaAtivaAtual.DATA_INICIO = VoCampanhaAtivaAtual.DATA_FINAL.AddDays(-1);
                    }
                    else {}

                    VoCampanhaAtivaAtual.ALTERADO_POR = "INTEGRACAO";
                    VoCampanhaAtivaAtual.DATA_ALTERACAO = DateTime.Now;

                    _ctx.Entry(VoCampanhaAtivaAtual).State = EntityState.Modified;
                    _ctx.SaveChanges();
                }

                if (!_ctx.voCampanhaAtiva.AsNoTracking().Where(p => p.CAMPANHA == novaCampanha.CAMPANHA && p.CANAL == novaCampanha.CANAL && p.REGIAO == novaCampanha.REGIAO && p.MARCA == novaCampanha.MARCA).Any())
                {
                    // CASO SEJA UMA CAMPANHA NOVA ADICIONA 
                    _ctx.voCampanhaAtiva.Add(novaCampanha);
                    _ctx.SaveChanges();
                }
            }                                             

            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            { 
                if (_ctx.voCampanha.AsNoTracking().FirstOrDefault(a => a.NOME_CAMPANHA == novaCampanha.CAMPANHA && a.MARCA == novaCampanha.MARCA) == null)
                {
                    Log.Write(String.Format($"{DateTime.Now} :: Nova Campanha Adicionada: [{novaCampanha.MARCA} - {novaCampanha.CAMPANHA}]"));
                    _ctx.Set<VO_CAMPANHA>().Add(
                        new VO_CAMPANHA
                        {
                            CANAL = novaCampanha.CANAL,
                            MARCA = novaCampanha.MARCA,
                            NOME_CAMPANHA = novaCampanha.CAMPANHA,
                            DESC_CAMPANHA = novaCampanha.CAMPANHA
                        }
                    );
                    _ctx.SaveChanges();
                    voImportacaoLog.STATUS = "INSERIDO";
                }
                else
                {
                    voImportacaoLog.STATUS = "NENHUMA_ACAO";
                    Log.Write(String.Format($"{DateTime.Now} :: Nenhuma Campanha Adicionada"));
                }

                ListaLogImportacao.Add(voImportacaoLog);
            }

            //ALTERANDO E ADICIONANDO CAMPANHA POR CLIENTE ========================================
            voImportacaoLog = null;

            Log.Write(String.Format($"{DateTime.Now} : Atribuindo campanha aos Clientes"));
             
            List<VO_CLIENTE> VoClientes;
   
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                VoClientes = _ctx.voCliente
                    .Where(a =>
                        a.COD_PORTFOLIO.ToUpper() == nomePortfolio.ToUpper()
                ).ToList();
            }

            foreach (VO_CLIENTE VoCliente in VoClientes)
            {
                VO_CAMPANHA_CLIENTE VoCampanhaClienteNova = new VO_CAMPANHA_CLIENTE();
                VO_CAMPANHA_CLIENTE VoCampanhaClienteAtual = new VO_CAMPANHA_CLIENTE();

                // LOG CAMPANHA NOVA
                voImportacaoLog = new VO_IMPORTACAO_LOG();
                
                //LOG CAMPANHA ANTIGA
                VO_IMPORTACAO_LOG voImportacaoLogAntiga = new VO_IMPORTACAO_LOG();
                
                try
                {
                    using ( DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        VoCampanhaClienteNova = new VO_CAMPANHA_CLIENTE
                        {
                            COD_CAMPANHA = _ctx.voCampanha.AsNoTracking().FirstOrDefault(a => a.NOME_CAMPANHA == novaCampanha.CAMPANHA).COD_CAMPANHA,
                            CODIGO_SAP = VoCliente.CODIGO_SAP,
                            DATA_INICIO = novaCampanha.DATA_INICIO,
                            DATA_FINAL = novaCampanha.DATA_FINAL
                        };
                    }

                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {                              
                        VoCampanhaClienteAtual = _ctx.voCampanhaCliente
                        .FirstOrDefault(a =>
                        a.CODIGO_SAP == VoCampanhaClienteNova.CODIGO_SAP
                        && a.DATA_FINAL >= VoCampanhaClienteNova.DATA_INICIO);

                        voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                        voImportacaoLog.NOME_ARQUIVO = arquivo;
                        voImportacaoLog.TIPO_IMPORTACAO = "CAMPANHA_CLIENTE";
                        voImportacaoLog.CODIGO_IDETIFICADOR = VoCampanhaClienteNova.COD_CAMPANHA+ "@" + portfolioLoccitane.CAMPANHA + "@" + VoCampanhaClienteNova.CODIGO_SAP;

                        
                        if (VoCampanhaClienteAtual == null)
                        {
                            Log.Write(String.Format($"{DateTime.Now} :: Cliente: [({VoCliente.UF}) {VoCliente.DOCUMENTO} - {VoCliente.NOME}]"));
                            Log.Write(String.Format($"{DateTime.Now} ::: Campanha ATIVADA: [{VoCampanhaClienteNova.COD_CAMPANHA} - {novaCampanha.CAMPANHA}]"));

                            _ctx.Set<VO_CAMPANHA_CLIENTE>().Add(VoCampanhaClienteNova);
                            _ctx.SaveChanges();
                            voImportacaoLog.STATUS = "ATIVADA";
                            ListaLogImportacao.Add(voImportacaoLog);
                        }
                        else 
                        {

                            if (VoCampanhaClienteNova.COD_CAMPANHA != VoCampanhaClienteAtual.COD_CAMPANHA)
                            {
                                // DADOS REFERENTES A CAMPANHA ANTIGA
                                voImportacaoLogAntiga.DATA_IMPORTACAO = InicioImportacao;
                                voImportacaoLogAntiga.NOME_ARQUIVO = arquivo;
                                voImportacaoLogAntiga.TIPO_IMPORTACAO = "CAMPANHA_CLIENTE";
                                voImportacaoLogAntiga.CODIGO_IDETIFICADOR = VoCampanhaClienteAtual.COD_CAMPANHA + "@" + VoCampanhaClienteAtual.VO_CAMPANHA.NOME_CAMPANHA + "@" + VoCampanhaClienteAtual.CODIGO_SAP;

                                // DESATIVA CAMPANHA ANTIGA
                                Log.Write(String.Format($"{DateTime.Now} :: Cliente: [({VoCliente.UF}) {VoCliente.DOCUMENTO} - {VoCliente.NOME}]"));
                                Log.Write(String.Format($"{DateTime.Now} ::: Campanha DESATIVADA: [{VoCampanhaClienteAtual.COD_CAMPANHA} - {VoCampanhaClienteAtual.VO_CAMPANHA.NOME_CAMPANHA}]"));
                                VoCampanhaClienteAtual.DATA_FINAL = VoCampanhaClienteNova.DATA_INICIO.AddDays(-1);

                                _ctx.Entry<VO_CAMPANHA_CLIENTE>(VoCampanhaClienteAtual).State = EntityState.Modified;
                                _ctx.SaveChanges();
                                voImportacaoLogAntiga.STATUS = "DESATIVADA";
                                ListaLogImportacao.Add(voImportacaoLogAntiga);

                                //ATIVA CAMPANHA NOVA
                                Log.Write(String.Format($"{DateTime.Now} ::: Campanha ATIVADA: [{VoCampanhaClienteNova.COD_CAMPANHA} - {novaCampanha.CAMPANHA}]"));
                                _ctx.Set<VO_CAMPANHA_CLIENTE>().Add(VoCampanhaClienteNova);
                                _ctx.SaveChanges();
                                voImportacaoLog.STATUS = "ATIVADA";
                                ListaLogImportacao.Add(voImportacaoLog);
                            }
                            else
                            {
                                // CASO FOR A MESMA CAMPANHA. ELE APENAS RENOVA
                                VoCampanhaClienteAtual.DATA_INICIO = VoCampanhaClienteNova.DATA_INICIO;
                                VoCampanhaClienteAtual.DATA_FINAL = VoCampanhaClienteNova.DATA_FINAL;

                                VoCampanhaClienteAtual.ALTERADO_POR = "INTEGRACAO";
                                VoCampanhaClienteAtual.DATA_ALTERACAO = DateTime.Now;

                                _ctx.Entry<VO_CAMPANHA_CLIENTE>(VoCampanhaClienteAtual).State = EntityState.Modified;
                                _ctx.SaveChanges();

                                voImportacaoLogAntiga.STATUS = "RENOVADA";
                                ListaLogImportacao.Add(voImportacaoLogAntiga);
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    ErrosImportacao++;
                    voImportacaoLog.STATUS = "ERRO";
                    ListaLogImportacao.Add(voImportacaoLog);

                    Log.Write(String.Format($"{DateTime.Now} : ERRO [CLIENTE: {VoCliente.CODIGO_SAP} | CAMP ATUAL: {VoCampanhaClienteAtual.COD_CAMPANHA} | CAMP NOVA {VoCampanhaClienteNova.COD_CAMPANHA}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
                    continue;
                }
            }

            return ListaLogImportacao;
        }
        private static void incluirPortfolio(VO_PORTFOLIO voPortfolio)
        {
            using(DataContextVarejonline _ctx = new DataContextVarejonline())
            {

                VO_PORTFOLIO oldVoPortfolio = _ctx.voPortfolio.AsNoTracking().Where(p => p.COD_PORTFOLIO == voPortfolio.COD_PORTFOLIO).FirstOrDefault();

                if (oldVoPortfolio != null )
                {
                    // NÃO MODIFICA A DATA DE CADASTRO DO ANTIGO REGISTRO. APENAS A DATA DE ALTERAÇÃO 
                    voPortfolio.DATA_CADASTRO = oldVoPortfolio.DATA_CADASTRO;
                    _ctx.Entry<VO_PORTFOLIO>(voPortfolio).State = EntityState.Modified;
                    _ctx.SaveChanges();

                    oldVoPortfolio = null;
                }
                else
                {
                    _ctx.Set<VO_PORTFOLIO>().Add(voPortfolio);
                    _ctx.SaveChanges();
                }
            }
        }
    }
}
