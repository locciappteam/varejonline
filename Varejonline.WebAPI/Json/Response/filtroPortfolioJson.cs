﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Varejonline.WebAPI.Json.Response
{
    public class filtroPortfolioJson
    {
        [JsonProperty("codigoSistema")]
        public string codigoSistema { get; set; }

        [JsonProperty("naMarcaDoCliente")]
        public int? naMarcaDoCliente { get; set; }

        [JsonProperty("noPortfolioDoCliente")]
        public int? noPortfolioDoCliente { get; set; }
        
        [JsonProperty("naTabelaDoCliente")]
        public int? naTabelaDoCliente { get; set; }

    }
}
