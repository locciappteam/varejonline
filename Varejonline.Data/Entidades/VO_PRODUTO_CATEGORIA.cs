﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_PRODUTO_CATEGORIA : VO_MODIFICACOES
    {
        public VO_PRODUTO_CATEGORIA()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, ForeignKey("VO_PRODUTO")]
        public string COD_PRODUTO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(50, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string MARCA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(50, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string LINHA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(50, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string SEGMENTO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_PRODUTO VO_PRODUTO { get; set; }
    }
}
