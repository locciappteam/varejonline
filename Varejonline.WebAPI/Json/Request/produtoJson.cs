﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Data.DataContext;

namespace Varejonline.WebAPI.Json.Request
{
    public class produtoJson
    {
        public produtoJson()
        {
            categorias = new List<Categoria>();
        }

        public bool ativo { get; set; }
        public string nome { get; set; }
        public string codigoSistema { get; set; }
        public string codigoBarras { get; set; }
        public string siglaUnidadePrincipal { get; set; }
        public List<Categoria> categorias { get; set; }
        public string ncm { get; set; }
        public decimal fatorCompra { get; set; }
        public string urlFoto { get; set; }
        public string observacoes { get; set; }
    }

    public class Categoria
    {
        public string nomeCategoria { get; set; }
        public string nomeNivel { get; set; }
    }
}
