﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Data.Entidades;

namespace Varejonline.Core.Emails
{
    public class MensagemHtmlLogTabelaPreco
    {
        public static string Formatar(List<VO_IMPORTACAO_LOG> voImportacaoLog)
        {

            string Mensagem = "<style>" +
                                    "table.blueTable{border:0px solid;width:100%;text-align:left;border-collapse:collapse;}" +

                                    "table.blueTable td, table.blueTable th{border:1px solid #AAAAAA;}" +
                                    "table.blueTable tbody td{font-size:13px;}" +
                                    "table.blueTable tr:nth-child(even){background:#D0E4F5;}" +

                                    "table.blueTable thead{background:#85e085;}" +
                                    "table.blueTable thead th{font-size:15px;font-weight:bold;color:#FFFFFF;border-left:2px solid #D0E4F5;}" +
                                    "table.blueTable thead th:first-child{border-left:none;}" +

                                    "table.blueTable tfoot{font-size:14px;font-weight:bold;color:#FFFFFF;background:#D0E4F5;background:-moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:-webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);border-top:2px solid #444444;}" +
                                    "table.blueTable tfoot td{font-size:14px;}" +
                                    "table.blueTable tfoot .links{text-align:right;}" +
                                    "table.blueTable tfoot .links a{display:inline-block;background:#1C6EA4;color:#FFFFFF;padding:2px 8px;border-radius:5px;}" +

                                    "table.blueTable caption{ color: white; background:gray;}" +

                                "</style>";

            var arquivos = from a in voImportacaoLog
                           group a by a.NOME_ARQUIVO into newGroup
                           select newGroup;

            foreach (var arquivo in arquivos)
            {

                Mensagem +=
                "<table class='blueTable'>" +
                "<caption> INFORMAÇÕES DA IMPORTAÇÃO DE TABELAS DE PREÇOS </caption>" +
                       "<thead>" +
                        string.Format($"<tr><td>NOME ARQUIVO</td><td> {arquivo.Select(a => a.NOME_ARQUIVO).FirstOrDefault()} </td></tr>") +
                       string.Format($"<tr><td>DATA IMPORTAÇÃO</td><td> {arquivo.Select(a => a.DATA_IMPORTACAO).FirstOrDefault()} </td></tr>") +
                   "</thead>" +
               "</table><br><br>";

                Mensagem +=
                "<table class='blueTable'>" +
                "<caption> TABELAS DE PREÇOS </caption>" +
                "<thead>" +
                        "<tr>" +
                            "<th>CÓDIGO TABELA</th>" +
                            "<th>STATUS</th>" +
                            "<th>DESCRIÇÃO ERRO</th>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>";
                foreach (VO_IMPORTACAO_LOG log in voImportacaoLog.Where(a => a.TIPO_IMPORTACAO == "TABELA_PRECO").OrderBy(a => a.STATUS))
                {

                    string CodTabela = log.CODIGO_IDETIFICADOR.Split('@')[0];

                    if (log.STATUS == "ERRO")
                    {
                        Mensagem +=
                        "<tr bgcolor= 'red' <font color='white'> " +
                            String.Format($"<td>{CodTabela}</td>") +
                            String.Format($"<td>{log.STATUS}</td>") +
                            String.Format($"<td>{log.DESC_ERRO}</td>") +
                        "</font></tr>";
                    }
                    else
                    {
                        Mensagem +=
                        "<tr>" +
                            String.Format($"<td>{CodTabela}</td>") +
                            String.Format($"<td>{log.STATUS}</td>") +
                            String.Format($"<td>{log.DESC_ERRO}</td>") +
                        "</tr>";
                    }

                }
                Mensagem += "</tbody>" + "</table><br><br>";

                Mensagem +=
                "<table class='blueTable'>" +
                "<caption> TABELAS ITENS PREÇOS </caption>" +
                "<thead>" +
                        "<tr>" +
                            "<th>CÓDIGO TABELA</th>" +
                            "<th>CÓDIGO PRODUTO</th>" +
                            "<th>STATUS</th>" +
                            "<th>DESCRIÇÃO ERRO</th>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>";

                foreach (VO_IMPORTACAO_LOG log in voImportacaoLog.Where(a => a.TIPO_IMPORTACAO == "TABELA_PRECO_ITEM").OrderBy(a => a.STATUS))
                {

                    string CodTabela    = log.CODIGO_IDETIFICADOR.Split('@')[0];
                    string CodProduto   = log.CODIGO_IDETIFICADOR.Split('@')[1];

                    if (log.STATUS == "ERRO")
                    {
                        Mensagem +=
                        "<tr bgcolor= 'red' <font color='white'> " +
                            String.Format($"<td>{CodTabela}</td>") +
                            String.Format($"<td>{CodProduto}</td>") +
                            String.Format($"<td>{log.STATUS}</td>") +
                            String.Format($"<td>{log.DESC_ERRO}</td>") +
                        "</font></tr>";
                    }
                    else
                    {
                        Mensagem +=
                        "<tr>" +
                            String.Format($"<td>{CodTabela}</td>") +
                            String.Format($"<td>{CodProduto}</td>") +
                            String.Format($"<td>{log.STATUS}</td>") +
                            String.Format($"<td>{log.DESC_ERRO}</td>") +
                        "</tr>";
                    }

                }
                Mensagem += "</tbody>" + "</table><br><br>";
            }
            return Mensagem;
        }
    }
}
