﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_PARAMETROS : VO_MODIFICACOES
    {
        [Key]
        public string PARAMETRO { get; set; }
        public string DESC_PARAMETRO { get; set; }
        public string VALOR { get; set; }
        public string TIPO { get; set; }
        public bool ATIVO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
