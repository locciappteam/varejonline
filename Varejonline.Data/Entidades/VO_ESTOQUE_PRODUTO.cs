﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_ESTOQUE_PRODUTO : VO_MODIFICACOES
    {
        public VO_ESTOQUE_PRODUTO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, Column(Order = 0), ForeignKey("VO_ESTOQUE_CONTROLE")]
        public int ESTOQUEID { get; set; }

        [Key, Column(Order = 1), ForeignKey("VO_PRODUTO")]
        [StringLength(15, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_PRODUTO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public int QUANTIDADE { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_PRODUTO VO_PRODUTO { get; set; }
        public virtual VO_ESTOQUE_CONTROLE VO_ESTOQUE_CONTROLE { get; set; }
    }
}
