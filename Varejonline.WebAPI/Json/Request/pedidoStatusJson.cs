﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.WebAPI.Json.Request
{
    public class pedidoStatusJson
    {
        public string numeroPedido { get; set; }
        public string status { get; set; }
    }
}
