﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Varejonline.Data.Entidades
{
    public class VO_IMPORTACAO_LOG
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public string NOME_ARQUIVO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public DateTime DATA_IMPORTACAO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public string TIPO_IMPORTACAO { get; set; }

        public string CODIGO_IDETIFICADOR { get; set; }

        public string STATUS { get; set; }

        public string DESC_ERRO { get; set; }
    }
}
