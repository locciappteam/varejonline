﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Request;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiTabelaPreco
    {
        private static List<tabelaPrecoJson> getTabelaPrecoJson(DateTime DataTransferencia)
        {
            List<tabelaPrecoJson> tabelasPreco;
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                tabelasPreco = (
                    from
                        voTabelaPreco in _ctx.voTabelaPreco
                    select new tabelaPrecoJson
                    {
                        ativa = voTabelaPreco.ATIVADO,
                        codigo = voTabelaPreco.COD_TABELA,
                        itens = (
                            from
                                voTabelaPrecoItems in voTabelaPreco.VO_TABELA_PRECO_ITEMs
                            where
                                voTabelaPrecoItems.DATA_ALTERACAO >= DataTransferencia
                            select new TabelaPrecoItem
                            {
                                codigoSistema = voTabelaPrecoItems.COD_PRODUTO,
                                valor = voTabelaPrecoItems.VALOR_UNITARIO,
                                ativo = voTabelaPrecoItems.ATIVO
                            }
                        ).ToList()
                    }
                )
                .Where(a => a.itens.Count > 0)
                .ToList();
            }

            return tabelasPreco;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia)
        {
            List<tabelaPrecoJson> TabelaPrecoJson = getTabelaPrecoJson(DataTransferencia);

            foreach (tabelaPrecoJson tabelaPreco in TabelaPrecoJson)
            { 
                List<TabelaPrecoItem> tabelaPrecoItens = tabelaPreco.itens;
                for (int i = 0; i < tabelaPrecoItens.Count(); i += 1000)
                {
                    try
                    {  
                        tabelaPreco.itens = tabelaPrecoItens.Skip(i).Take(1000).ToList();

                        string jsonRequest = JsonConvert.SerializeObject(tabelaPreco);
                        string responseData;

                       Task.Run(async () =>
                        {
                            responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);
                            saveResponse(tabelaPreco.codigo, responseData);
                        }).GetAwaiter().GetResult();
                    }
                    catch (Exception Ex)
                     {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }

        private static void saveResponse(string CodTabelaPreco, string responseData)
        {
            Log.Write(String.Format($"{DateTime.Now} : Salvando Retorno WebAPI"));

            if (responseData != "Falho")
            {
                responseJson ReponseJson = JsonConvert.DeserializeObject<responseJson>(responseData);
                string[] DescErro = ReponseJson.errorDescription.Split(';');

                List<VO_TABELA_PRECO_RETORNO> VoTabelaPrecoRetorno = new List<VO_TABELA_PRECO_RETORNO>();
                foreach (var retornosuccessProcessed in ReponseJson.successProcessed)
                {
                    VoTabelaPrecoRetorno.Add(new VO_TABELA_PRECO_RETORNO
                    {
                        COD_TABELA = CodTabelaPreco,
                        COD_PRODUTO = retornosuccessProcessed,
                        RETORNO_SUCESSO = "Retorno com sucesso!",
                        RETORNO_FALHO = null,
                        DESC_ERRO = null,
                    });
                }

                foreach (var retornofailedProcessed in ReponseJson.failedProcessed)
                {
                    VoTabelaPrecoRetorno.Add(new VO_TABELA_PRECO_RETORNO
                    {
                        COD_TABELA = CodTabelaPreco,
                        COD_PRODUTO = retornofailedProcessed,
                        RETORNO_SUCESSO = null,
                        RETORNO_FALHO = "Retorno com erros!",
                        DESC_ERRO = DescErro[ReponseJson.failedProcessed.IndexOf(retornofailedProcessed)],
                    });
                }

                foreach (var retorno in VoTabelaPrecoRetorno.Distinct())
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_TABELA_PRECO_RETORNO>().Add(retorno);
                            _ctx.SaveChanges();
                        }
                    }
                    catch (Exception Ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO SALVAR RETORNO [{retorno.COD_TABELA}-{retorno.COD_PRODUTO}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }
    }
}
