﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Data.Entidades;

namespace Varejonline.Core.Emails
{
    public class MensagemHTML
    {
        public static string Formatar(List<VO_PEDIDO_CAPA> voPedidoCapa)
        {
            string Mensagem = "<style>table.blueTable{border:1px solid #1C6EA4;background-color:#EEEEEE;width:100%;text-align:left;border-collapse:collapse;}table.blueTable td, table.blueTable th{border:1px solid #AAAAAA;padding:3px 2px;}table.blueTable tbody td{font-size:13px;}table.blueTable tr:nth-child(even){background:#D0E4F5;}table.blueTable thead{background:#1C6EA4;background:-moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);background:-webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1C6EA4 100%);background:linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1C6EA4 100%);border-bottom:2px solid #444444;}table.blueTable thead th{font-size:15px;font-weight:bold;color:#FFFFFF;border-left:2px solid #D0E4F5;}table.blueTable thead th:first-child{border-left:none;}table.blueTable tfoot{font-size:14px;font-weight:bold;color:#FFFFFF;background:#D0E4F5;background:-moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:-webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);border-top:2px solid #444444;}table.blueTable tfoot td{font-size:14px;}table.blueTable tfoot .links{text-align:right;}table.blueTable tfoot .links a{display:inline-block;background:#1C6EA4;color:#FFFFFF;padding:2px 8px;border-radius:5px;}</style>" +
            "<table class='blueTable'>" +
                "<thead>" +
                    "<tr>" +
                        "<th>PEDIDO</th>" +
                        "<th>CNPJ</th>" +
                        "<th>RAZÃO SOCIAL</th>" +
                        "<th>MARCA</th>" +
                        "<th>DATA COMPRA</th>" +
                        "<th>VALOR TOTAL</th>" +
                        "<th>DESCONTO</th>" +
                        "<th>VALOR LIQUIDO</th>" +
                        "<th>OBSERVACAO</th>" +
                    "</tr>" +
                "</thead>" +
                "<tbody>";
                    foreach (VO_PEDIDO_CAPA Pedido in voPedidoCapa)
                    {
                        Mensagem +=
                        "<tr>" +
                            String.Format($"<td>{Pedido.NUMERO}</td>") +
                            String.Format($"<td>{Pedido.CODIGO_SAP}</td>") +
                            String.Format($"<td>{Pedido.VO_CLIENTE.NOME}</td>") +
                            String.Format($"<td>{Pedido.VO_CLIENTE.MARCA}</td>") +
                            String.Format($"<td>{Pedido.DATA_COMPRA}</td>") +
                            String.Format($"<td>{Pedido.VALOR_TOTAL}</td>") +
                            String.Format($"<td>{Pedido.VALOR_DESCONTO}</td>") +
                            String.Format($"<td>{Pedido.VALOR_LIQUIDO}</td>") +
                            String.Format($"<td>{Pedido.OBSERVACAO}</td>") +
                        "</tr>";
                    }
                    Mensagem +=
              "</tbody>" +
            "</table>";

            return Mensagem;
        }
    }
}
