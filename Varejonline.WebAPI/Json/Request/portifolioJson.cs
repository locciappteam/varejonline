﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Data.DataContext;

namespace Varejonline.WebAPI.Json.Request
{
    public class portifolioJson
    {
        public portifolioJson()
        {
            portifolioProdutos = new List<portifolioProdutosJson>();
        }

        public string codigoPortfolio { get; set; }
        public List<portifolioProdutosJson> portifolioProdutos { get; set; }
    }
    public class portifolioProdutosJson
    {
        public string codigoPortfolio { get; set; }
        public string codigoSistema { get; set; }
        public int compraMaxima { get; set; }
        public decimal fatorCompra { get; set; }
        public string observacao { get; set; }
        public string nivelDestaque { get; set; }
    }
}
