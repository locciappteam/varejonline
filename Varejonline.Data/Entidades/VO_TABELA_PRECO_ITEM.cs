﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_TABELA_PRECO_ITEM : VO_MODIFICACOES
    {
        public VO_TABELA_PRECO_ITEM()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;            
        }

        [Key, Column(Order = 0), ForeignKey("VO_TABELA_PRECO")]
        public string COD_TABELA { get; set; }

        [Key, Column(Order = 1), ForeignKey("VO_PRODUTO")]
        public string COD_PRODUTO { get; set; }
        public decimal VALOR_UNITARIO { get; set; }
        public bool ATIVO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_TABELA_PRECO VO_TABELA_PRECO { get; set; }
        public virtual VO_PRODUTO VO_PRODUTO { get; set; }
    }
}
