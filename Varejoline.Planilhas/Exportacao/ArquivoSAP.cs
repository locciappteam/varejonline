﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;

namespace Varejonline.Planilhas.Exportacao
{
    public class ArquivoSAP
    {
        private static DataContextVarejonline _ctx = new DataContextVarejonline();

        public static void GerarPlanilha(string caminhoArquivo, string nomeArquivo, int canalSAP, string marcaLoccitane = null)
        {
            int ErrosExportacao = 0;

            List<int> vo_canais = _ctx.voCanal.Where(p => p.CANAL_SAP == canalSAP).Select(p => p.COD_CANAL).ToList();
             
            List<VO_PEDIDO_CAPA> VoPedidoCapa = _ctx.voPedidoCapa
                    .Where(a => vo_canais.Contains(a.VO_CLIENTE.CANAL) && a.PEDIDO_GERADO == false
                    ).ToList();

            if (VoPedidoCapa.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : GERANDO PLANILHA DE PEDIDOS [{nomeArquivo}]"));

                foreach (var Pedido in VoPedidoCapa)
                { 
                    Log.Write(String.Format($"{DateTime.Now} :: Pedido [{Pedido.NUMERO}]"));

                    int index = 1;

                    XLWorkbook workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add("Pedidos");

                    var realNomeArquivo = String.Format($"{nomeArquivo}_{Pedido.NUMERO.Replace("-","")}{DateTime.Now.ToString("_dd.MM.yyyy_hh.mm")}");

                    try
                    {
                        string DescricaoPedido = (
                            canalSAP == 20 ? 
                                String.IsNullOrEmpty(Pedido.TIPO_COMPRA) || String.IsNullOrEmpty(Pedido.CAMPANHA_VINGENTE) 
                                    ? "FRANQUIA" 
                                    : String.Format($"{Pedido.TIPO_COMPRA.Substring(0, 4)} {Regex.Replace(Regex.Replace(Pedido.CAMPANHA_VINGENTE, "CAMPANHA", "", RegexOptions.IgnoreCase), "CAMP", "", RegexOptions.IgnoreCase)}") 

                            : canalSAP == 30 ? "ATACADO"
                            : canalSAP == 70 || canalSAP == 80 ? "B2B" 
                            : "PEDIDO"
                        );

                        foreach (var Item in Pedido.VO_PEDIDO_ITEMs)
                        {
                            worksheet.Cell(String.Format($"A{index}")).Value = "YASO";
                            worksheet.Cell(String.Format($"B{index}")).Value = "321";

                            if (Pedido.VO_CLIENTE.CANAL.ToString() != Pedido.VO_CLIENTE.VO_TABELA_PRECO.COD_TABELA)
                                worksheet.Cell(String.Format($"C{index}")).Value = canalSAP;
                            else
                                worksheet.Cell(String.Format($"C{index}")).Value = Pedido.VO_CLIENTE.VO_TABELA_PRECO.COD_TIPO_TABELA;

                            worksheet.Cell(String.Format($"D{index}")).Value = Pedido.VO_CLIENTE.CODIGO_SAP;
                            worksheet.Cell(String.Format($"E{index}")).Value = Pedido.VO_CLIENTE.CODIGO_SAP;
                            worksheet.Cell(String.Format($"F{index}")).Value = DateTime.Today.ToString("yyyyMMdd");
                            worksheet.Cell(String.Format($"G{index}")).Value = DescricaoPedido + " " + Pedido.NUMERO + " " + Pedido.VO_CLIENTE.APELIDO;
                            worksheet.Cell(String.Format($"H{index}")).Value = "";
                            worksheet.Cell(String.Format($"I{index}")).Value = "";
                            worksheet.Cell(String.Format($"J{index}")).Value = "";
                            worksheet.Cell(String.Format($"K{index}")).Value = Item.COD_PRODUTO;
                            worksheet.Cell(String.Format($"L{index}")).Value = Item.QUANTIDADE;
                            worksheet.Cell(String.Format($"M{index}")).Value = "3211";

                            index++;
                        }

                        using (DataContextVarejonline _ctx2 = new DataContextVarejonline())
                        {
                            VO_PEDIDO_ARQUIVO VoPedidoArquivo = new VO_PEDIDO_ARQUIVO
                            {
                                NUMERO = Pedido.NUMERO,
                                NOME_ARQUIVO = realNomeArquivo,
                                LOCAL_ARQUIVO = caminhoArquivo,
                                CADASTRADO_POR = "INTEGRACAO",
                                DATA_CADASTRO = DateTime.Now,
                                ALTERADO_POR = "INTEGRACAO",
                                DATA_ALTERACAO = DateTime.Now
                            };

                            _ctx2.Set<VO_PEDIDO_ARQUIVO>().Add(VoPedidoArquivo);
                            _ctx2.SaveChanges();
                        };

                        Pedido.PEDIDO_GERADO = true;
                        _ctx.Entry<VO_PEDIDO_CAPA>(Pedido).State = EntityState.Modified;
                        _ctx.SaveChanges();
                    }
                    catch (Exception Ex)
                    {
                        ErrosExportacao += ErrosExportacao;
                        Log.Write(String.Format($"{DateTime.Now} : ERRO [PEDIDO: {Pedido.NUMERO}] - ADVERTÊNCIA(S) [{Ex.Message}]"));

                        continue;
                    }

                    workbook.SaveAs($@"{caminhoArquivo}{realNomeArquivo}.xlsx");
                }

                if (ErrosExportacao > 0)
                {
                    Email.Enviar(
                            "am03svc-cegid@loccitane.com.br",
                            "dlbr-itapplications@loccitane.com",
                            "Erro ao gerar arquivo para o SAP. Verifique o LOG.",
                            "Erro ao exportar Planilha - SAP"
                    );
                }
            }
        }

        public static void GerarPlanilhaV2(string caminhoArquivo)
        {
            int ErrosExportacao = 0;

            // LISTA TODOS OS CANAIS
            List<VO_CANAL> listaCanais = _ctx.voCanal.AsNoTracking().ToList();
            
            // LISTA TODOS OS PEDIDOS QUE OS ARQUIVOS NÃO FORAM GERADOS 
            List<VO_PEDIDO_CAPA>  listaPedidos = _ctx.voPedidoCapa.AsNoTracking().Where(a=> a.PEDIDO_GERADO == false).ToList();

            // FAZ UMA LISTA DE PEDIDOS GERADOS X CLIENTES X CANAIS
            var novalistaPedidos = (
                                        from ped in listaPedidos
                                        from cli in _ctx.voCliente.AsNoTracking().ToList().Where(p => p.CODIGO_SAP == ped.CODIGO_SAP)
                                        join can in listaCanais on cli.CANAL equals can.COD_CANAL
                                        select new
                                        {
                                            ped.NUMERO,
                                            ped.CODIGO_SAP,
                                            ped.DATA_COMPRA,
                                            ped.VALOR_TOTAL,
                                            ped.VALOR_DESCONTO,
                                            ped.OBSERVACAO,
                                            ped.VALOR_LIQUIDO,
                                            ped.STATUS,
                                            ped.PEDIDO_GERADO,
                                            ped.CAMPANHA_VINGENTE,
                                            ped.TIPO_COMPRA,
                                            ped.EMAIL_ENVIADO,

                                            ped.VO_CLIENTE,
                                            ped.VO_PEDIDO_ARQUIVO,
                                            ped.VO_PEDIDO_ITEMs,
                                            ped.VO_PEDIDO_RETORNOs,

                                            ped.CADASTRADO_POR,
                                            ped.DATA_CADASTRO,
                                            ped.ALTERADO_POR,
                                            ped.DATA_ALTERACAO,

                                            can.CANAL_SAP,
                                            can.COD_CANAL,
                                            can.ABREVIACAO,
                                            can.NOME_ARQUIVO,
                                            can.TEM_CAMPANHA
                                        }

                                   ).ToList();

            if (novalistaPedidos.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : GERANDO PLANILHA DE PEDIDOS"));

                foreach (var Pedido in novalistaPedidos)
                {
                    var marca = Pedido.VO_CLIENTE.MARCA.ToUpper().Contains("PROVENCE")?"_EnProvence" 
                                :Pedido.VO_CLIENTE.MARCA.ToUpper().Contains("BRESIL") ? "_AuBresil" 
                                :"";
                    
                    var nomeArquivo = String.Format($"{Pedido.NOME_ARQUIVO + marca}_{Pedido.NUMERO.Replace("-", "")}{DateTime.Now.ToString("_dd.MM.yyyy_hh.mm")}");

                    Log.Write(String.Format($"{DateTime.Now} :: Pedido [{Pedido.NUMERO}]"));

                    int index = 1;

                    XLWorkbook workbook = new XLWorkbook();
                    var worksheet = workbook.Worksheets.Add("Pedidos");

                    try
                    {
                        string DescricaoPedido = Pedido.ABREVIACAO.Substring(0, 3);

                        if (Pedido.TEM_CAMPANHA)
                        {
                            if(Pedido.CAMPANHA_VINGENTE != null)
                                DescricaoPedido += String.Format($" {Pedido.TIPO_COMPRA.Substring(0, 4)} {Regex.Replace(Regex.Replace(Pedido.CAMPANHA_VINGENTE, "CAMPANHA", "", RegexOptions.IgnoreCase), "CAMP", "", RegexOptions.IgnoreCase)}");
                        }

                        foreach (var Item in Pedido.VO_PEDIDO_ITEMs)
                        {
                            worksheet.Cell(String.Format($"A{index}")).Value = "YASO";
                            worksheet.Cell(String.Format($"B{index}")).Value = "321";
                            worksheet.Cell(String.Format($"C{index}")).Value = Pedido.CANAL_SAP;
                            worksheet.Cell(String.Format($"D{index}")).Value = Pedido.VO_CLIENTE.CODIGO_SAP;
                            worksheet.Cell(String.Format($"E{index}")).Value = Pedido.VO_CLIENTE.CODIGO_SAP;
                            worksheet.Cell(String.Format($"F{index}")).Value = DateTime.Today.ToString("yyyyMMdd");
                            worksheet.Cell(String.Format($"G{index}")).Value = DescricaoPedido + " " + Pedido.NUMERO + " " + Pedido.VO_CLIENTE.APELIDO;
                            worksheet.Cell(String.Format($"H{index}")).Value = "";
                            worksheet.Cell(String.Format($"I{index}")).Value = "";
                            worksheet.Cell(String.Format($"J{index}")).Value = "";
                            worksheet.Cell(String.Format($"K{index}")).Value = Item.COD_PRODUTO;
                            worksheet.Cell(String.Format($"L{index}")).Value = Item.QUANTIDADE;
                            worksheet.Cell(String.Format($"M{index}")).Value = "3211";

                            index++;
                        }

                        using (DataContextVarejonline _ctx2 = new DataContextVarejonline())
                        {
                            VO_PEDIDO_ARQUIVO VoPedidoArquivo = new VO_PEDIDO_ARQUIVO
                            {
                                NUMERO = Pedido.NUMERO,
                                NOME_ARQUIVO = nomeArquivo,
                                LOCAL_ARQUIVO = caminhoArquivo,
                                CADASTRADO_POR = "INTEGRACAO",
                                DATA_CADASTRO = DateTime.Now,
                                ALTERADO_POR = "INTEGRACAO",
                                DATA_ALTERACAO = DateTime.Now
                            };

                            _ctx2.Set<VO_PEDIDO_ARQUIVO>().Add(VoPedidoArquivo);
                            _ctx2.SaveChanges();
                        };

                        _ctx.Entry(
                            new VO_PEDIDO_CAPA {
                                NUMERO = Pedido.NUMERO,
                                CODIGO_SAP = Pedido.CODIGO_SAP,
                                DATA_COMPRA = Pedido.DATA_COMPRA,
                                VALOR_TOTAL = Pedido.VALOR_TOTAL,
                                VALOR_DESCONTO = Pedido.VALOR_DESCONTO,
                                OBSERVACAO = Pedido.OBSERVACAO,
                                VALOR_LIQUIDO = Pedido.VALOR_LIQUIDO,
                                STATUS = Pedido.STATUS,
                                PEDIDO_GERADO = true,
                                CAMPANHA_VINGENTE = Pedido.CAMPANHA_VINGENTE,
                                TIPO_COMPRA = Pedido.TIPO_COMPRA,
                                EMAIL_ENVIADO = Pedido.EMAIL_ENVIADO,
                                ALTERADO_POR = Pedido.ALTERADO_POR,
                                DATA_ALTERACAO = Pedido.DATA_ALTERACAO
                            }).State = EntityState.Modified;
                        
                        _ctx.SaveChanges();
                    }
                    catch (Exception Ex)
                    {
                        ErrosExportacao += ErrosExportacao;
                        Log.Write(String.Format($"{DateTime.Now} : ERRO [PEDIDO: {Pedido.NUMERO}] - ADVERTÊNCIA(S) [{Ex.Message}]"));

                        continue;
                    }

                    workbook.SaveAs($@"{caminhoArquivo}{nomeArquivo}.xlsx");
                }

                if (ErrosExportacao > 0)
                {
                    Email.Enviar(
                            "am03svc-cegid@loccitane.com.br",
                            "dlbr-itapplications@loccitane.com",
                            "Erro ao gerar arquivo para o SAP. Verifique o LOG.",
                            "Erro ao exportar Planilha - SAP"
                    );
                }
            }
        }
    }
}
