﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_PORTFOLIO_PRODUTO : VO_MODIFICACOES
    {
        public VO_PORTFOLIO_PRODUTO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, Column(Order = 0), ForeignKey("VO_PORTFOLIO")]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_PORTFOLIO { get; set; }

        [Key, Column(Order = 1), ForeignKey("VO_PRODUTO")]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(15, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_PRODUTO { get; set; }

        public int COMPRA_MAXIMA { get; set; }
        public decimal FATOR_COMPRA { get; set; }

        [StringLength(50, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string NIVEL_DESTAQUE { get; set; }

        [StringLength(150, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string OBSERVACAO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_PORTFOLIO VO_PORTFOLIO { get; set; }
        public virtual VO_PRODUTO VO_PRODUTO { get; set; }
    }
}
