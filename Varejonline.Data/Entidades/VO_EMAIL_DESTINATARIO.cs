﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_EMAIL_DESTINATARIO : VO_MODIFICACOES
    {
        public VO_EMAIL_DESTINATARIO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public string EMAIL { get; set; }
        public string NOME { get; set; }
        public bool FRANQUIA { get; set; }
        public bool ATACADO { get; set; }
        public bool SPA { get; set; }
        public bool B2B { get; set; }
        public bool SEMANAL { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
        public bool REL_PORTFOLIO { get; set; }
        public bool REL_PRODUTO {get;set;}
        public bool REL_TABPRECO { get; set; }
        public bool REL_ESTOQUE { get; set; }
        public bool REL_CLIENTE { get; set; }
    }
}
