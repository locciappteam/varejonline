﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;

namespace Varejonline.Planilhas.Importacao
{
    public class produtoCsv
    {
        public static void ImportarDoCegid()
        {
            try
            {
                Log.Write(String.Format($"{DateTime.Now} :: Importação Automática do Cegid"));
                using (var _ctx = new DataContextVarejonline())
                {
                    _ctx.Database.ExecuteSqlCommand("UPDATE_PRODUTO_CATEGORIA_FROM_CEGID");
                }
                Log.Write(String.Format($"{DateTime.Now} :: Finalizando Importação Automática do Cegid"));
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} :: Erro: {ex.Message}"));
            }
        }
        
        public static void Importar(string pathArquivo, string arquivoCsv)
        {
            int ErrosImportacao = 0;
            DateTime InicioImportacao = DateTime.Now;
            string novoNomeArquivo = String.Format($"{Path.GetFileNameWithoutExtension(arquivoCsv)}_{InicioImportacao.ToString("dd.MM.yyyy_hh.mm")}.csv");
            List<VO_IMPORTACAO_LOG> ListaLogImportacao = new List<VO_IMPORTACAO_LOG>();

            if (File.Exists(pathArquivo + arquivoCsv)) 
            {
                Log.Write(String.Format($"{DateTime.Now} :: Importando Planilha CSV - {arquivoCsv}"));

                int i=0;
                foreach (string line in File.ReadAllLines(pathArquivo + arquivoCsv, Encoding.GetEncoding("iso-8859-1")).Skip(1))
                {
                    VO_IMPORTACAO_LOG voImportacaoLog = new VO_IMPORTACAO_LOG();
                    voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                    voImportacaoLog.TIPO_IMPORTACAO = "PRODUTO";
                    voImportacaoLog.NOME_ARQUIVO = novoNomeArquivo;

                    try
                    {
                        i++;

                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            string CodProduto = line.Split(';')[0];

                            VO_PRODUTO VoProduto = new VO_PRODUTO
                            {
                                COD_PRODUTO = line.Split(';')[0],
                                COD_BARRA = line.Split(';')[1],
                                NOME = line.Split(';')[2],
                                UNID_PRINCIPAL = line.Split(';')[3],
                                NCM = line.Split(';')[4],

                                VO_PRODUTO_CATEGORIA = new VO_PRODUTO_CATEGORIA
                                {
                                    COD_PRODUTO = line.Split(';')[0],
                                    MARCA = line.Split(';')[6],
                                    LINHA = line.Split(';')[7],
                                    SEGMENTO = line.Split(';')[8]
                                }
                            };

                            voImportacaoLog.CODIGO_IDETIFICADOR = VoProduto.COD_PRODUTO;

                            if(_ctx.voProduto.AsNoTracking().FirstOrDefault(a => a.COD_PRODUTO == CodProduto) == null)
                            {
                                _ctx.Set<VO_PRODUTO>().Add(VoProduto);
                                _ctx.SaveChanges();
                                voImportacaoLog.STATUS = "INSERIDO";
                            }
                            else
                            {
                                _ctx.Entry(VoProduto).State = EntityState.Modified;
                                _ctx.Entry(VoProduto.VO_PRODUTO_CATEGORIA).State = EntityState.Modified;
                                _ctx.SaveChanges();

                                voImportacaoLog.STATUS = "ATUALIZADO";
                            }

                        }

                        ListaLogImportacao.Add(voImportacaoLog);
                    }
                    catch (Exception Ex)
                    {
                        ErrosImportacao++;

                        Log.Write(String.Format($"{DateTime.Now} : ERRO [LINHA: {i} | PRODUTO: {line.Split(';')[0]}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
                        voImportacaoLog.STATUS = "ERRO";
                        voImportacaoLog.DESC_ERRO = Ex.Message;
                        ListaLogImportacao.Add(voImportacaoLog);
                        continue;
                    }
                }
                
                //MOVE ARQUIVO PARA PASTA DE PROCESSADO ===============================================     
                Log.Write(String.Format($"{DateTime.Now} :: Enviando para pasta Processado - Produto {arquivoCsv}"));
                File.Move(
                    pathArquivo + arquivoCsv,
                    String.Format($@"{pathArquivo}\Processado\{novoNomeArquivo}")
                );
 
                foreach (var log in ListaLogImportacao)
                {
                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        _ctx.Set<VO_IMPORTACAO_LOG>().Add(log);
                        _ctx.SaveChanges();
                    }
                }

                //RELATÓRIO DE IMPORTAÇÃO DE PLANILHA 
                List<string> VoEmailDestinatario = new DataContextVarejonline().voEmailDestinatario
                    .AsNoTracking()
                    .Where(a => a.REL_PRODUTO == true)
                    .Select(a => a.EMAIL)
                    .ToList();

                Email.Enviar(
                           "am03svc-cegid@loccitane.com.br",String.Join(",",VoEmailDestinatario),
                           MensagemHtmlLogProduto.Formatar(ListaLogImportacao),
                           String.Format($"Relatório Importação Planilha - Produto {novoNomeArquivo}"),
                           true
                   );
            }
        }
    }
}
