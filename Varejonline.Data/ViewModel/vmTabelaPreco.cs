﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.ViewModel
{
    public class vmTabelaPreco
    {
        public string COD_TABELA { get; set; }
        public int COD_TIPO_TABELA { get; set; }       
        public bool ATIVADO { get; set; }
        public string UF { get; set; }
        public string COD_PRODUTO { get; set; }
        public decimal VALOR_UNITARIO { get; set; }
        public bool PRODUTO_ATIVADO { get; set; }
    }
}
