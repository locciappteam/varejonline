﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public interface VO_MODIFICACOES
    {
        string CADASTRADO_POR { get; set; }
        DateTime DATA_CADASTRO { get; set; }
        string ALTERADO_POR { get; set; }
        DateTime DATA_ALTERACAO { get; set; }
    }
}
