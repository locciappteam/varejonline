﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;

namespace Varejonline.Planilhas.Importacao
{
    public class clienteCsv
    {
        public static void Importar(string pathArquivo, string arquivoCsv)
        {
            
            int ErrosImportacao = 0;

            if (File.Exists(pathArquivo + arquivoCsv))
            {
                int i = 0;
                DateTime InicioImportacao = DateTime.Now;
                List<VO_IMPORTACAO_LOG> ListaLogImportacao = new List<VO_IMPORTACAO_LOG>();
                string novoNomeArquivo = String.Format($"{Path.GetFileNameWithoutExtension(arquivoCsv)}_{InicioImportacao.ToString("dd.MM.yyyy_hh.mm")}.csv");

                List<VO_CANAL> canais = new DataContextVarejonline().voCanal.AsNoTracking().ToList();

                Log.Write(String.Format($"{DateTime.Now} :: Importando Planilha CSV - {arquivoCsv}"));
                foreach (string line in File.ReadLines(pathArquivo + arquivoCsv, Encoding.GetEncoding("iso-8859-1")).Skip(1))
                {
                    i++;
                    VO_CLIENTE VoCliente = new VO_CLIENTE();
                    VO_IMPORTACAO_LOG voImportacaoLog = new VO_IMPORTACAO_LOG();

                    try
                    {

                        // TRATAMENTO DE CANAIS 
                        int canal_SAP = Convert.ToInt32(line.Split(';')[15]);
                        string categoria_Canal = line.Split(';')[16];

                        VoCliente = new VO_CLIENTE();

                        VoCliente.UF = line.Split(';')[0];
                        VoCliente.CODIGO_SAP = line.Split(';')[1];
                        VoCliente.DOCUMENTO = line.Split(';')[2];
                        VoCliente.NOME = line.Split(';')[3];
                        VoCliente.APELIDO = line.Split(';')[4];
                        VoCliente.GRUPO_ECONOMICO = line.Split(';')[5];
                        VoCliente.COD_PORTFOLIO = line.Split(';')[6];
                        VoCliente.COD_TABELA = line.Split(';')[7];
                        VoCliente.UTILIZA_FATOR_COMPRA = (line.Split(';')[8] == "S" ? true : false);
                        VoCliente.DESCONTO_PADRAO = (string.IsNullOrEmpty(line.Split(';')[9]) ? 0 : Convert.ToDecimal(line.Split(';')[9].Replace('.', ',')));
                        VoCliente.EMAIL = (line.Split(';')[10]).ToLower().Trim();
                        VoCliente.MARCA = line.Split(';')[11];
                        VoCliente.PERCENTUAL_PARTICIPACAO = (string.IsNullOrEmpty(line.Split(';')[12]) ? 100 : Convert.ToDecimal(line.Split(';')[12].Replace('.', ',')));
                        VoCliente.VALOR_MINIMO_PEDIDO = (string.IsNullOrEmpty(line.Split(';')[13]) ? 0 : Convert.ToDecimal(line.Split(';')[13].Replace('.', ',')));
                        VoCliente.FRANQUIA = (line.Split(';')[14] == "S" ? true : false);
                        VoCliente.CANAL = canais.Where(p => p.CANAL_SAP == canal_SAP && p.CATEGORIA_CANAL == categoria_Canal).Single().COD_CANAL;
                        VoCliente.ATIVO = (line.Split(';')[17] == "N" ? false : true);
                        VoCliente.DATA_CADASTRO = DateTime.Now;

                        VoCliente.VO_CLIENTE_ENDERECO = new VO_CLIENTE_ENDERECO
                        {
                            CODIGO_SAP = line.Split(';')[1],
                            CEP = "04711130",
                            NUMERO = "1240",
                            CADASTRADO_POR = "INTEGRACAO",
                            DATA_CADASTRO = DateTime.Now,
                            ALTERADO_POR = "INTEGRACAO",
                            DATA_ALTERACAO = DateTime.Now
                        };

                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            VO_CLIENTE voClienteBD = _ctx.voCliente.AsNoTracking().FirstOrDefault(a => a.CODIGO_SAP == VoCliente.CODIGO_SAP);

                            voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                            voImportacaoLog.NOME_ARQUIVO = novoNomeArquivo;
                            voImportacaoLog.TIPO_IMPORTACAO = "CLIENTE";
                            voImportacaoLog.CODIGO_IDETIFICADOR = VoCliente.CODIGO_SAP;

                            if (voClienteBD == null)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : ADICIONANDO NOVO CLIENTE: [({line.Split(';')[0]}) {line.Split(';')[1]} - {line.Split(';')[2]}]"));

                                _ctx.Set<VO_CLIENTE>().Add(VoCliente);
                                _ctx.SaveChanges();

                                voImportacaoLog.STATUS = "INSERIDO";
                                ListaLogImportacao.Add(voImportacaoLog);

                                if (VoCliente.CANAL == 20)
                                {
                                    AtribuirCampanha(VoCliente);
                                }
                            }
                            else
                            {
                                // O CLIENTE FOI ENCONTRADO POR ISSO AS INFORMAÇÕES DE CADASTRO INICIAL SÃO MANTIDAS 
                                VoCliente.DATA_CADASTRO = voClienteBD.DATA_CADASTRO;
                                VoCliente.CADASTRADO_POR = voClienteBD.CADASTRADO_POR;

                                voClienteBD = null;

                                _ctx.Entry(VoCliente).State = EntityState.Modified;
                                _ctx.SaveChanges();

                                voImportacaoLog.STATUS = "ATUALIZADO";
                                ListaLogImportacao.Add(voImportacaoLog);
                            }
                        }
                    }
                    catch (Exception Ex)
                    {

                        voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                        voImportacaoLog.NOME_ARQUIVO = novoNomeArquivo;
                        voImportacaoLog.TIPO_IMPORTACAO = "CLIENTE";
                        voImportacaoLog.CODIGO_IDETIFICADOR = VoCliente.CODIGO_SAP == "" ? "????" : VoCliente.CODIGO_SAP ;

                        ErrosImportacao ++;
                        voImportacaoLog.STATUS = "ERRO";

                        // Traducao Erro
                        string erro = Ex.Message;

                        if (erro.Contains("FK_VO_TABELA_PRECO_VO_CLIENTE"))
                        {
                            erro += " TABELA DE PREÇO INEXISTENTE";
                        }

                        // ---------
                        voImportacaoLog.DESC_ERRO = erro;
                        ListaLogImportacao.Add(voImportacaoLog);

                        Log.Write(String.Format($"{DateTime.Now} : ERRO [LINHA: {i} | CLIENTE: {VoCliente.DOCUMENTO} - {VoCliente.NOME}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
                        continue;
                    }
                }

                foreach (var log in ListaLogImportacao)
                {
                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        _ctx.Set<VO_IMPORTACAO_LOG>().Add(log);
                        _ctx.SaveChanges();
                    }
                }

                //RELATÓRIO DE IMPORTAÇÃO DE PLANILHA 
                List<string> VoEmailDestinatario = new DataContextVarejonline().voEmailDestinatario
                             .AsNoTracking()
                             .Where(a => a.REL_CLIENTE == true)
                             .Select(a => a.EMAIL)
                             .ToList();

                Email.Enviar(
                           "am03svc-cegid@loccitane.com.br", String.Join(",", VoEmailDestinatario),
                           MensagemHtmlLogCliente.Formatar(ListaLogImportacao),
                           String.Format($"Relatório Importação Planilha - Clientes {novoNomeArquivo}"),
                           true
                   );

                //MOVE ARQUIVO PARA PASTA DE PROCESSADO ============================================
                Log.Write(String.Format($"{DateTime.Now} :: Enviando para pasta Processado - Cliente {arquivoCsv}"));
                File.Move(
                    pathArquivo + arquivoCsv,
                    String.Format($@"{pathArquivo}\Processado\{novoNomeArquivo}")
                );
            }
            else
            {
                Log.Write(String.Format($"{DateTime.Now} :: Planilha CSV - Não localizada {pathArquivo + arquivoCsv}"));
            }
        }

        public static void AtribuirCampanha(VO_CLIENTE VoCliente)
        {
            List<string> UfNorte = new List<string>(new string[] { "AM", "RR", "AP", "PA", "TO", "RO", "AC" });

            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                DateTime DataAtual = DateTime.Today;
                List<VO_CAMPANHA_ATIVA> VoCampanhaAtiva = _ctx.voCampanhaAtiva
                    .Where(a =>
                        a.MARCA == VoCliente.MARCA
                        && a.CANAL == VoCliente.CANAL
                        && a.DATA_FINAL >= DataAtual
                        && a.REGIAO.ToUpper() == (UfNorte.Contains(VoCliente.UF) ? "NORTE" : "OUTROS")
                    ).ToList();

                if (VoCampanhaAtiva.Any())
                {
                    foreach(VO_CAMPANHA_ATIVA CampanhaAtiva in VoCampanhaAtiva)
                    {
                        int CodCampanha = _ctx.voCampanha.FirstOrDefault(a => a.NOME_CAMPANHA == CampanhaAtiva.CAMPANHA).COD_CAMPANHA;

                        Log.Write(String.Format($"{DateTime.Now} : Atribuindo Campanha ao Cliente: {CodCampanha}"));
                        VO_CAMPANHA_CLIENTE VoCampanhaCliente = new VO_CAMPANHA_CLIENTE
                        {
                            COD_CAMPANHA = CodCampanha,
                            CODIGO_SAP = VoCliente.CODIGO_SAP,
                            DATA_INICIO = CampanhaAtiva.DATA_INICIO,
                            DATA_FINAL = CampanhaAtiva.DATA_FINAL
                        };

                        _ctx.Set<VO_CAMPANHA_CLIENTE>().Add(VoCampanhaCliente);
                        _ctx.SaveChanges();
                    }
                    
                }
                else
                {
                    Log.Write(String.Format($"{DateTime.Now} : Nenhuma Campanha Atribuida ao Cliente"));
                }
            }
        }
    }
}