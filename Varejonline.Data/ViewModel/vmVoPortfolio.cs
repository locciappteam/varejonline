﻿using System;

namespace Varejonline.Data.ViewModel
{
    public class vmVoPortfolio
    {
        public int CANAL { get; set; }
        public int CANAL_SAP { get; set; }
        public string CATEGORIA_CANAL { get; set; }
        public string MARCA { get; set; }
        public string CAMPANHA { get; set; }
        public DateTime DATA_INICIO { get; set; }
        public DateTime DATA_FINAL { get; set; }
        public string REGIAO { get; set; }
    }
}
