﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Varejonline.Data.Entidades
{
    public class VO_CLIENTE_TELEFONE : VO_MODIFICACOES
    {
        public VO_CLIENTE_TELEFONE()
        {
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, ForeignKey("VO_CLIENTE")]
        public string CODIGO_SAP { get; set; }

        public int? DDI { get; set; }
        public int? DDD { get; set; }
        public string NUMERO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_CLIENTE VO_CLIENTE { get; set; }
    }
}
