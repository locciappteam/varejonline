﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Timers;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Core.Relatorios;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.Planilhas.Exportacao;
using Varejonline.Planilhas.Importacao;
using Varejonline.WebAPI.WebAPI.GET;
using Varejonline.WebAPI.WebAPI.POST;

namespace Varejonline.WinService
{
    public class TimerVarejonline
    {
        static Timer timer;

        public static void TimeScheduler()
        {
            try
            {
                Log.Write(strMensagem: String.Format($"{DateTime.Now} : Realizando agendamento..."), Tipo: 0);

                DateTime ScheduledTime = DateTime.Now.AddMinutes(30);

                Log.Write(String.Format($"{DateTime.Now} : Tarefa agendada para {ScheduledTime}"));
                                                          
                double TickTime = (double)(ScheduledTime - DateTime.Now).TotalMilliseconds;
                timer = new Timer(TickTime);
                timer.Elapsed += new ElapsedEventHandler(TimeElapsed);
                timer.Start();
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : Erro Fatal: {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "dlbr-itapplications@loccitane.com", ex.ToString());
            }
        }

        private static void TimeElapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            Log.Write(String.Format($"{DateTime.Now} : Iniciando a tarefa..."));

            try
            {
                //CULTURE PT-BR =======================================================================
                System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");

                //GET PARAMETROS ======================================================================
                Console.WriteLine("OBTENDO PARAMETROS =================================================");

                List<VO_PARAMETROS> VoParametros;
                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    VoParametros = _ctx.voParametros.Where(a => a.ATIVO == true).ToList();
                }

                //ATRIBUINDO PARAMETROS ===============================================================
                string dataInicial = DateTime.Today.AddDays(-10).ToShortDateString();
                string UltimoDiaMes = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1).ToShortDateString();
                string PathImportacao = VoParametros.FirstOrDefault(a => a.PARAMETRO == "PATH_IMPORTACAO").VALOR;
                string PathExportacao = VoParametros.FirstOrDefault(a => a.PARAMETRO == "PATH_EXPORTACAO").VALOR;
                int TimeOutWebApi = Convert.ToInt32(VoParametros.FirstOrDefault(a => a.PARAMETRO == "TIMEOUT_WEBAPI").VALOR);
                DateTime DataTransferencia = Convert.ToDateTime(VoParametros.FirstOrDefault(a => a.PARAMETRO == "DATA_TRANSFERENCIA").VALOR);

                //IDENTIFICA AMBIENTE DE TRABALHO =====================================================
                string User;
                string Pass;
                Uri baseAddress;

                //PRODUÇÃO
                if (VoParametros.FirstOrDefault(a => a.PARAMETRO == "AMBIENTE").VALOR == "1")
                {
                    User = VoParametros.FirstOrDefault(a => a.PARAMETRO == "USUARIO_API").VALOR;
                    Pass = VoParametros.FirstOrDefault(a => a.PARAMETRO == "SENHA_API").VALOR;
                    baseAddress = new Uri(VoParametros.FirstOrDefault(a => a.PARAMETRO == "BASE_ADDRESS").VALOR);
                }
                //HOMOLOGAÇÃO
                else
                {
                    User = VoParametros.FirstOrDefault(a => a.PARAMETRO == "USUARIO_API_HOM").VALOR;
                    Pass = VoParametros.FirstOrDefault(a => a.PARAMETRO == "SENHA_API_HOM").VALOR;
                    baseAddress = new Uri(VoParametros.FirstOrDefault(a => a.PARAMETRO == "BASE_ADDRESS_HOM").VALOR);
                }
                byte[] byteAuth = Encoding.ASCII.GetBytes($"{User}:{Pass}");
                string Auth = Convert.ToBase64String(byteAuth);

                //IMPORTAÇÃO DE PLANILHAS CSV =========================================================
                produtoCsv.ImportarDoCegid();
                produtoCsv.Importar(PathImportacao, "Produto.csv");

                portfolioCsv.Importar(PathImportacao);

                tabelaPrecoCsv.Importar(PathImportacao, "Tabela_Preco.csv");

                clienteCsv.Importar(PathImportacao, "Clientes.csv");

                estoqueCsv.Importar(PathImportacao, "Estoque.csv");

                //WEB API =============================================================================
                apiPedidos.Get(Auth, baseAddress, String.Format("order?desde={0}&ate={1}", dataInicial, UltimoDiaMes), TimeOutWebApi);

                apiStatusPedido.Post(Auth, baseAddress, "order/status", TimeOutWebApi, DataTransferencia, "Pedido");

                apiProdutos.Post(Auth, baseAddress, "product", TimeOutWebApi, DataTransferencia);

                apiEstoque.Post(Auth, baseAddress, "stock", TimeOutWebApi, DataTransferencia);

                apiPortfolio.Post(Auth, baseAddress, "portfolio", TimeOutWebApi, DataTransferencia);

                apiTabelaPreco.Post(Auth, baseAddress, "price", 20, DataTransferencia);

                apiClientes.Post(Auth, baseAddress, "customer", TimeOutWebApi, DataTransferencia);

                apiCampanhas.Post(Auth, baseAddress, "campanha", TimeOutWebApi, DataTransferencia);

                //GERA PEDIDO SAP =====================================================================

                ArquivoSAP.GerarPlanilhaV2(PathExportacao);

                /*
                ArquivoSAP.GerarPlanilha(PathExportacao, "SAP_AuBresil", 20, "LOCCITANE AU BRESIL");

                ArquivoSAP.GerarPlanilha(PathExportacao, "SAP_EnProvence", 20, "LOCCITANE EN PROVENCE");

                ArquivoSAP.GerarPlanilha(PathExportacao, "SAP_Franquia", 20);

                ArquivoSAP.GerarPlanilha(PathExportacao, "SAP_Atacado", 30);

                ArquivoSAP.GerarPlanilha(PathExportacao, "SAP_B2B", 70);

                ArquivoSAP.GerarPlanilha(PathExportacao, "SAP_B2B", 80);
                */

                //ENVIA RELATORIOS ====================================================================

                //Envia novos Pedidos 
                RelatorioNovoPedido.Enviar();

                if (DateTime.Today.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour == 10)
                {
                    Console.WriteLine(">>> Semanal");
                    RelatorioSemanal.Enviar();
                }

                //ATUALIZA PARAMETRO DATA_TRANSFERENCIA ===============================================
                Console.WriteLine("ATUALIZANDO PARAMETROS =============================================");

                Console.WriteLine(">>> DATA_TRANSFERENCIA");
                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    VO_PARAMETROS pDataTransferencia = VoParametros.FirstOrDefault(a => a.PARAMETRO == "DATA_TRANSFERENCIA");
                    pDataTransferencia.VALOR = DateTime.Now.ToString();

                    _ctx.Entry<VO_PARAMETROS>(pDataTransferencia).State = EntityState.Modified;
                    _ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : Erro Fatal: {ex.ToString()}"));
                Email.Enviar("am03svc-cegid@loccitane.com.br", "dlbr-itapplications@loccitane.com", ex.ToString());
            }

            Log.Write(String.Format($"{DateTime.Now} : Fim da tarefa..."));
            TimeScheduler();
        }
    }
}
