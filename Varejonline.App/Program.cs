﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using Varejonline.Core.Relatorios;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.Planilhas.Exportacao;
using Varejonline.Planilhas.Importacao;
using Varejonline.WebAPI.WebAPI.GET;
using Varejonline.WebAPI.WebAPI.POST;

namespace Varejonline.App
{
    class Program
    {
        static void Main( )
        {
            //RelatorioNovoPedido.Enviar();

            //CULTURE PT-BR =======================================================================
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");  

            //GET PARAMETROS ======================================================================
            Console.WriteLine("OBTENDO PARAMETROS =================================================");

            List<VO_PARAMETROS> VoParametros;
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                 VoParametros = _ctx.voParametros.Where(a => a.ATIVO == true).ToList();
            }

            //ATRIBUINDO PARAMETROS ===============================================================
            string Menos30Dias = DateTime.Today.AddDays(-10).ToShortDateString();
            string UltimoDiaMes = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1).ToShortDateString();

            //string Menos30Dias = new DateTime(2021,03,01).ToString();
            //string UltimoDiaMes = new DateTime(2021, 03,11).ToString();

            string PathImportacao = VoParametros.FirstOrDefault(a => a.PARAMETRO == "PATH_IMPORTACAO").VALOR;
            string PathExportacao = VoParametros.FirstOrDefault(a => a.PARAMETRO == "PATH_EXPORTACAO").VALOR;
            int TimeOutWebApi = Convert.ToInt32(VoParametros.FirstOrDefault(a => a.PARAMETRO == "TIMEOUT_WEBAPI").VALOR);


            DateTime DataTransferencia = Convert.ToDateTime(VoParametros.FirstOrDefault(a => a.PARAMETRO == "DATA_TRANSFERENCIA").VALOR);
            //DataTransferencia = Convert.ToDateTime("2020-01-09 17:15");

            //IDENTIFICA AMBIENTE DE TRABALHO =====================================================
            string User;
            string Pass;
            Uri baseAddress;

            //PRODUÇÃO
            if (VoParametros.FirstOrDefault(a => a.PARAMETRO == "AMBIENTE").VALOR == "1")
            {
                User = VoParametros.FirstOrDefault(a => a.PARAMETRO == "USUARIO_API").VALOR;
                Pass = VoParametros.FirstOrDefault(a => a.PARAMETRO == "SENHA_API").VALOR;
                baseAddress = new Uri(VoParametros.FirstOrDefault(a => a.PARAMETRO == "BASE_ADDRESS").VALOR);
            }
            //HOMOLOGAÇÃO
            else
            {     
                User = VoParametros.FirstOrDefault(a => a.PARAMETRO == "USUARIO_API_HOM").VALOR;
                Pass = VoParametros.FirstOrDefault(a => a.PARAMETRO == "SENHA_API_HOM").VALOR;
                baseAddress = new Uri(VoParametros.FirstOrDefault(a => a.PARAMETRO == "BASE_ADDRESS_HOM").VALOR);
            }

            byte[] byteAuth = Encoding.ASCII.GetBytes($"{User}:{Pass}");
            string Auth = Convert.ToBase64String(byteAuth);

            //IMPORTAÇÃO DE PLANILHAS CSV =========================================================
            Console.WriteLine("IMPORTANDO PLANILHAS ===============================================");

           // Console.WriteLine(">>> Produtos");
            //produtoCsv.ImportarDoCegid();
            //produtoCsv.Importar(PathImportacao, "Produto.csv");

           // Console.WriteLine(">>> Portfolio");
            //portfolioCsv.Importar(PathImportacao);

           // Console.WriteLine(">>> Tabela de Preço");
           //tabelaPrecoCsv.Importar(PathImportacao, "Tabela_Preco.csv");

            //Console.WriteLine(">>> Clientes");
            //clienteCsv.Importar(PathImportacao, "Clientes.csv");

            //Console.WriteLine(">>> Estoque");
            //estoqueCsv.Importar(PathImportacao, "Estoque.csv");

            //WEB API =============================================================================
            //Console.WriteLine("ENVIANDO E RECEBENDO DADOS WEB API =================================");

            //Console.WriteLine(">>> Obtendo Pedidos");
            //apiPedidos.Get(Auth, baseAddress, String.Format("order?desde={0}&ate={1}", Menos30Dias, UltimoDiaMes), TimeOutWebApi);

            //Console.WriteLine(">>> Status Pedido");
            //apiStatusPedido.Post(Auth, baseAddress, "order/status", TimeOutWebApi, DataTransferencia, "Pedido");

            //Console.WriteLine(">>> Produtos");
            //apiProdutos.Post(Auth, baseAddress, "product", TimeOutWebApi, DataTransferencia);

            //Console.WriteLine(">>> Estoque");
            //apiEstoque.Post(Auth, baseAddress, "stock", TimeOutWebApi, DataTransferencia);
            
            //Console.WriteLine(">>> Portfólio");            
            //apiPortfolio.Post(Auth, baseAddress, "portfolio", TimeOutWebApi, DataTransferencia);

            //Console.WriteLine(">>> Tabela de Preço");
            //apiTabelaPreco.Post(Auth, baseAddress, "price", 20, DataTransferencia);

            //Console.WriteLine(">>> Clientes"); 
            apiClientes.Post(Auth, baseAddress, "customer", TimeOutWebApi, DataTransferencia);

            //Console.WriteLine(">>> Campanha");
            //apiCampanhas.Post(Auth, baseAddress, "campanha", TimeOutWebApi, DataTransferencia);

            //GERA PEDIDO SAP =====================================================================
            //Console.WriteLine("GERANDO ARQUIVO SAP ================================================");
            //ArquivoSAP.GerarPlanilhaV2(PathExportacao);
          
            //ENVIA RELATORIOS ==================================================================== 
         RelatorioNovoPedido.Enviar();
           
            /*
            if (DateTime.Today.DayOfWeek == DayOfWeek.Friday && DateTime.Now.Hour == 10)
            {

                Console.WriteLine(">>> Semanal");
                RelatorioSemanal.Enviar();          
            }
            */

            //ATUALIZA PARAMETRO DATA_TRANSFERENCIA ===============================================
            Console.WriteLine("ATUALIZANDO PARAMETROS =============================================");

            Console.WriteLine(">>> DATA_TRANSFERENCIA");
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                VO_PARAMETROS pDataTransferencia = VoParametros.FirstOrDefault(a => a.PARAMETRO == "DATA_TRANSFERENCIA");
                pDataTransferencia.VALOR = DateTime.Now.ToString();

                _ctx.Entry<VO_PARAMETROS>(pDataTransferencia).State = EntityState.Modified;
                _ctx.SaveChanges();
            }
        }
    }
   
}
