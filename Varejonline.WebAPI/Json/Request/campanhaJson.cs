﻿using Newtonsoft.Json;
using System;

namespace Varejonline.WebAPI.Json.Request
{
    public class campanhaJson
    {
        public string campanha { get; set; }

        [JsonIgnore]
        public DateTime? inicio { get; set; }
        [JsonProperty("inicio")]
        public string inicio_str
        {
            get
            {
                return inicio != null ? inicio.Value.ToString("dd-MM-yyyy hh:mm:ss") : null;
            }
            set
            {
                inicio = string.IsNullOrEmpty(value) ? default(DateTime?) : DateTime.ParseExact(value, "dd-MM-yyyy hh:mm:ss", null);
            }
        }

        [JsonIgnore]
        public DateTime? fim { get; set; }
        [JsonProperty("fim")]
        public string fim_str
        {
            get
            {
                return fim != null ? fim.Value.ToString("dd-MM-yyyy hh:mm:ss") : null;
            }
            set
            {
                fim = string.IsNullOrEmpty(value) ? default(DateTime?) : DateTime.ParseExact(value, "dd-MM-yyyy hh:mm:ss", null);
            }
        }

        public string cliente { get; set; }

        public string canal { get; set; }
    }
}
