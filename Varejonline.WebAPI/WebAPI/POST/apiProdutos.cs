﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Request;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiProdutos
    {
        private static List<produtoJson> getProdutoJson(DateTime DataTransferencia)
        {
            string[] Colunas = new string[] { "MARCA", "LINHA", "SEGMENTO" };

            List<produtoJson> produtos;
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                produtos = (
                    from
                        produto in _ctx.voProduto
                    where
                        produto.DATA_ALTERACAO >= DataTransferencia
                    select new produtoJson
                    {
                        ativo = produto.ATIVADO,
                        nome = produto.NOME,
                        codigoSistema = produto.COD_PRODUTO,
                        codigoBarras = produto.COD_BARRA,
                        siglaUnidadePrincipal = produto.UNID_PRINCIPAL,
                        ncm = produto.NCM,

                        urlFoto = produto.URL_FOTO,
                        observacoes = null,
                        categorias = (
                            from
                                coluna in Colunas
                            select new Categoria
                            {
                                nomeCategoria = (
                                    coluna == "MARCA" ? produto.VO_PRODUTO_CATEGORIA.MARCA :
                                    coluna == "LINHA" ? produto.VO_PRODUTO_CATEGORIA.LINHA :
                                    coluna == "SEGMENTO" ? produto.VO_PRODUTO_CATEGORIA.SEGMENTO :
                                    null
                                ),
                                nomeNivel = coluna
                            }
                        ).ToList()
                    }
                ).ToList();
            }

            return produtos;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia)
        {
            List<produtoJson> ProdutoJson = getProdutoJson(DataTransferencia);

            for (int i = 0; i < ProdutoJson.Count(); i += 100)
            {
                try
                {
                    string jsonRequest = JsonConvert.SerializeObject(ProdutoJson.Skip(i).Take(100));
                    string responseData;

                    Task.Run(async () =>
                    {
                        responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);                    
                        saveResponse(responseData);
                    }).GetAwaiter().GetResult();
                }
                catch(Exception Ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                    continue;
                }
            }
        }

        private static void saveResponse(string responseData)
        {
            Log.Write(String.Format($"{DateTime.Now} : Salvando Retorno WebAPI"));

            if (responseData != "Falho")
            {
                responseJson ReponseJson = JsonConvert.DeserializeObject<responseJson>(responseData);
                string[] DescErro = ReponseJson.errorDescription.Split(';');

                List<VO_PRODUTO_RETORNO> VoProdutoRetorno = new List<VO_PRODUTO_RETORNO>();
                foreach (var retornosuccessProcessed in ReponseJson.successProcessed)
                {
                    VoProdutoRetorno.Add(new VO_PRODUTO_RETORNO
                    {
                        COD_PRODUTO = retornosuccessProcessed,
                        RETORNO_SUCESSO = "Retorno com sucesso!",
                        RETORNO_FALHO = null,
                        DESC_ERRO = null
                    });
                }

                foreach (var retornofailedProcessed in ReponseJson.failedProcessed)
                {
                    VoProdutoRetorno.Add(new VO_PRODUTO_RETORNO
                    {
                        COD_PRODUTO = retornofailedProcessed,
                        RETORNO_SUCESSO = null,
                        RETORNO_FALHO = "Retorno com erro!",
                        DESC_ERRO = DescErro[ReponseJson.failedProcessed.IndexOf(retornofailedProcessed)]
                    });
                }

                foreach (var retorno in VoProdutoRetorno.Distinct())
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_PRODUTO_RETORNO>().Add(retorno);
                            _ctx.SaveChanges();
                        }
                    }
                    catch (Exception Ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO SALVAR RETORNO [{retorno.COD_PRODUTO}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }
    }
}
