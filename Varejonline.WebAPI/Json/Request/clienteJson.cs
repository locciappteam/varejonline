﻿using System.Collections.Generic;

namespace Varejonline.WebAPI.Json.Request
{
    public class clienteJson
    {
        public string grupoEconomico { get; set; }
        
        public bool isFranquia { get; set; }
        public string nome { get; set; }
        public string documento { get; set; }
        public Telefone? telefone { get; set; }
        public Celular? celular { get; set; }
        public string email { get; set; }
        public Endereco? endereco { get; set; }
        public bool ativo { get; set; }        
        public List<Canal> canais { get; set; }
    }

    public class Telefone
    {
        public int? ddd { get; set; }
        public int? ddi { get; set; }
        public string numero { get; set; }
    }

    public class Celular
    {
        public int? ddd { get; set; }
        public int? ddi { get; set; }
        public string numero { get; set; }
    }

    public class Endereco
    {
        public string cep { get; set; }
        public string numero { get; set; }
    }

    public class Canal
    {
        public string canal { get; set; }
        public string marca { get; set; }

        public string tabelaPreco { get; set; }
        public string codigoPortfolio { get; set; }
        public decimal descontoPadrao { get; set; }
        public bool usaFatorCompra { get; set; }
        public decimal percentualParticipacao { get; set; }
        public decimal valorMinimoPedido { get; set; }
    }
}
