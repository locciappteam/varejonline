﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Request;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiStatusPedido
    {
        private static List<pedidoStatusJson> getStatusPedido(string status)
        {
            List<pedidoStatusJson> statusPedido;
            using(DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                statusPedido = (
                    from
                        a in _ctx.voPedidoCapa
                    where
                        a.STATUS == null
                    select new pedidoStatusJson
                    {
                        numeroPedido = a.NUMERO,
                        status = status
                    }
                ).ToList();
            }

            return statusPedido;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia, string status)
        {
            List<pedidoStatusJson> statusPedidoJson = getStatusPedido(status);

            foreach (var pedidoStatus in statusPedidoJson)
            {
                try
                {
                    string jsonRequest = JsonConvert.SerializeObject(pedidoStatus);
                    string responseData;

                    Task.Run(async () =>
                    {
                        responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);
                        saveResponse(pedidoStatus, responseData);
                    }).GetAwaiter().GetResult(); 
                }
                catch (Exception Ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                    continue;
                }
            }
        }

        public static void saveResponse (pedidoStatusJson pedidoStatus, string responseData)
        {
            if (responseData != "Falho")
            {
                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    VO_PEDIDO_CAPA VoPedidoCapa = _ctx.voPedidoCapa.Find(pedidoStatus.numeroPedido);
                    VoPedidoCapa.STATUS = pedidoStatus.status;

                    _ctx.Entry<VO_PEDIDO_CAPA>(VoPedidoCapa).State = EntityState.Modified;
                    _ctx.SaveChanges();
                }
            }
        }

    }
}
