﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Data.DataContext;

namespace Varejonline.WebAPI.Json.Request
{
    public class tabelaPrecoJson
    {
        public tabelaPrecoJson()
        {
            itens = new List<TabelaPrecoItem>();
        }

        public bool ativa { get; set; }
        public string codigo { get; set; }
        public List<TabelaPrecoItem> itens { get; set; }
    }

    public class TabelaPrecoItem
    {
        public string codigoSistema { get; set; }
        public decimal valor { get; set; }
        public bool ativo { get; set; }
    }
}
