﻿namespace Varejonline.WinService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerVarejoOnline = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerVarejoOnline = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerVarejoOnline
            // 
            this.serviceProcessInstallerVarejoOnline.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstallerVarejoOnline.Password = null;
            this.serviceProcessInstallerVarejoOnline.Username = null;
            // 
            // serviceInstallerVarejoOnline
            // 
            this.serviceInstallerVarejoOnline.Description = "Consome o Web API da Verejo Online";
            this.serviceInstallerVarejoOnline.DisplayName = "LOcc - Varejo Online";
            this.serviceInstallerVarejoOnline.ServiceName = "ServiceVarejoOnline";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerVarejoOnline,
            this.serviceInstallerVarejoOnline});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerVarejoOnline;
        private System.ServiceProcess.ServiceInstaller serviceInstallerVarejoOnline;
    }
}