﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.Data.Entidades.Comparators;
using Varejonline.WebAPI.Json.Request;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiClientes
    {
        private static List<clienteJson> getClienteJson(DateTime DataTransferencia)
        {
            List<clienteJson> clientes = new List<clienteJson>();

            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {

                var clientesDB = (
                                 from lis in _ctx.voCliente.AsNoTracking()
                                 select lis
                                 ).ToList();

                var canalDB = (_ctx.voCanal.AsNoTracking().ToList());

                var teste = (from list in clientesDB
                             where list.DATA_ALTERACAO >= DataTransferencia
                             select list
                             ).ToList();


                clientes = (from list in clientesDB
                            where list.DATA_ALTERACAO >= DataTransferencia
                            select new clienteJson
                            {
                                grupoEconomico = list.GRUPO_ECONOMICO,
                                isFranquia = list.FRANQUIA,
                                nome = list.NOME,
                                documento = list.DOCUMENTO,

                                /*
                                 * telefone = new Telefone {
                                    ddd = list.VO_CLIENTE_TELEFONE.DDD,
                                    ddi = list.VO_CLIENTE_TELEFONE.DDI,
                                    numero = list.VO_CLIENTE_TELEFONE.NUMERO
                                },
                                */

                                /*
                                celular = new Celular {
                                    ddi = list.VO_CLIENTE_CELULAR.DDI,
                                    ddd = list.VO_CLIENTE_CELULAR.DDD,
                                    numero = list.VO_CLIENTE_CELULAR.NUMERO
                                },
                                */

                                email = list.EMAIL,
                                endereco = new Endereco
                                {
                                    cep = list.VO_CLIENTE_ENDERECO.CEP,
                                    numero = list.VO_CLIENTE_ENDERECO.NUMERO
                                },

                                ativo = list.ATIVO,

                                canais = (from cli in clientesDB
                                          join can in canalDB on cli.CANAL equals can.COD_CANAL
                                          where cli.DOCUMENTO == list.DOCUMENTO && cli.ATIVO == true // só clientes ativos

                                          select new Canal
                                          {
                                              canal = cli.CANAL.ToString() + "-" + can.ABREVIACAO,
                                              marca = cli.MARCA,
                                              tabelaPreco = cli.COD_TABELA,
                                              codigoPortfolio = cli.COD_PORTFOLIO,
                                              descontoPadrao = cli.DESCONTO_PADRAO,
                                              usaFatorCompra = cli.UTILIZA_FATOR_COMPRA,
                                              percentualParticipacao = (decimal)cli.PERCENTUAL_PARTICIPACAO,
                                              valorMinimoPedido = (decimal)cli.VALOR_MINIMO_PEDIDO == null ?  0 : (decimal)cli.VALOR_MINIMO_PEDIDO
                                          }).ToList()
                            }
                             ).Distinct(new ComparadorGenerico<clienteJson>(
                                 (documento1, documento2) => documento1.documento == documento2.documento,
                                 documento => documento.documento.GetHashCode())
                             ).ToList();
            }

            return clientes;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia)
        {
            List<clienteJson> ClienteJson = getClienteJson(DataTransferencia);

            for (int i = 0; i < ClienteJson.Count(); i += 100)
            {
                try
                {
                    string jsonRequest = JsonConvert.SerializeObject(ClienteJson.Skip(i).Take(100));
                    string responseData;

                    Task.Run(async () =>
                    {
                        responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);
                         saveResponse(responseData);
                    }).GetAwaiter().GetResult();
                }
                catch (Exception Ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                    continue;
                }
            }
        }

        private static void saveResponse(string responseData)
        {
            if (responseData != "Falho")
            {
                responseJson ReponseJson = JsonConvert.DeserializeObject<responseJson>(responseData);
                string[] DescErro = ReponseJson.errorDescription.Split(';');

                List<VO_CLIENTE_RETORNO> VoClienteRetorno = new List<VO_CLIENTE_RETORNO>();
                foreach (var retornosuccessProcessed in ReponseJson.successProcessed)
                {
                    var CODIGO_SAP = new DataContextVarejonline().voCliente.Where(p => p.DOCUMENTO == retornosuccessProcessed).Select(p => p.CODIGO_SAP).FirstOrDefault();

                    VoClienteRetorno.Add(new VO_CLIENTE_RETORNO
                    {
                        CODIGO_SAP = CODIGO_SAP,
                        RETORNO_SUCESSO = "Retorno com sucesso!",
                        RETORNO_FALHO = null,
                        DESC_ERRO = null,
                    });
                }

                foreach (var retornofailedProcessed in ReponseJson.failedProcessed)
                {

                    var CODIGO_SAP = new DataContextVarejonline().voCliente.Where(p => p.DOCUMENTO == retornofailedProcessed).Select(p => p.CODIGO_SAP).FirstOrDefault();

                    VoClienteRetorno.Add(new VO_CLIENTE_RETORNO
                    {

                        CODIGO_SAP = CODIGO_SAP,
                        RETORNO_SUCESSO = null,
                        RETORNO_FALHO = "Retorno com erros!",
                        DESC_ERRO = DescErro[ReponseJson.failedProcessed.IndexOf(retornofailedProcessed)],
                    });
                }

                foreach (var retorno in VoClienteRetorno.Distinct())
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_CLIENTE_RETORNO>().Add(retorno);
                            _ctx.SaveChanges();
                        }
                    }
                    catch (Exception Ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO SALVAR RETORNO [{retorno.CODIGO_SAP}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }
    }
}
