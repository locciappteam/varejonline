﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.WebAPI.Json.Response
{
    public class responseJson
    {
        public List<string> successProcessed { get; set; }
        public List<string> failedProcessed { get; set; }
        public string errorDescription { get; set; }
    }
}
