﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Varejonline.Data.Entidades
{
    public class VO_PEDIDO_CAPA : VO_MODIFICACOES
    {
        public VO_PEDIDO_CAPA()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public string NUMERO { get; set; }

        [ForeignKey("VO_CLIENTE")]
        public string CODIGO_SAP { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public DateTime DATA_COMPRA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_TOTAL { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_DESCONTO { get; set; }

        public string OBSERVACAO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_LIQUIDO { get; set; }
        public string STATUS { get; set; }
        public bool PEDIDO_GERADO { get; set; }
        public string CAMPANHA_VINGENTE { get; set; }
        public string TIPO_COMPRA { get; set; }
        public bool EMAIL_ENVIADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<VO_PEDIDO_ITEM> VO_PEDIDO_ITEMs { get; set; }
        public virtual ICollection<VO_PEDIDO_RETORNO> VO_PEDIDO_RETORNOs { get; set; }
        public virtual VO_PEDIDO_ARQUIVO VO_PEDIDO_ARQUIVO { get; set; }
        public virtual VO_CLIENTE VO_CLIENTE { get; set; }
    }
}
