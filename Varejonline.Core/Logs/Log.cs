﻿using System;
using System.IO;
using System.Reflection;

namespace Varejonline.Core.Logs
{
    public class Log
    {
        private static string caminhoExe = string.Empty;
        public static bool Write(string strMensagem, string strNomeArquivo = "LogVarejoOnline.txt", int Tipo = 1)
        {
            try
            {
                caminhoExe = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string caminhoArquivo = Path.Combine(caminhoExe, strNomeArquivo);
                if (!File.Exists(caminhoArquivo))
                {
                    FileStream arquivo = File.Create(caminhoArquivo);
                    arquivo.Close();
                }
                using (StreamWriter w = File.AppendText(caminhoArquivo))
                {
                    if (Tipo == 0)
                    {
                        AppendLog(strMensagem, w);
                    }
                    else
                    {
                        w.WriteLine($"  :{strMensagem}");
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private static void AppendLog(string logMensagem, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Varejo Online : ");
                txtWriter.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine($"  :{logMensagem}");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
