﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_CAMPANHA_ATIVA : VO_MODIFICACOES
    {
        public VO_CAMPANHA_ATIVA()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, Column(Order = 0) ]
        public int CANAL { get; set; }

        [Key, Column(Order = 1)]
        public string MARCA { get; set; }

        [Key, Column(Order = 2)]
        public string REGIAO { get; set; }

        [Key, Column(Order = 3)]
        public string CAMPANHA { get; set; }
        public DateTime DATA_INICIO { get; set; }
        public DateTime DATA_FINAL { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }
    }
}
