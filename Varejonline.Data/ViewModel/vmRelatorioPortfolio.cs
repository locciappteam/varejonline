﻿using System;

namespace Varejonline.Data.ViewModel
{
    public class vmRelatorioPortfolio
    {
		public DateTime DATA_IMPORTACAO { get; set; }
		public string NOME_ARQUIVO { get; set; }
		public string COD_PORTFOLIO { get; set; }
		public string COD_PRODUTO { get; set; }
		public string STATUS_IMPORTACAO { get; set; }
		public string DESC_ERRO { get; set; }
		public string STATUS_VAREJONLINE { get; set; }

		public bool? NOPORTFOLIO_VAREJONLINE { get; set; }
		public bool? NATABELA_PRECO_VAREJOLINE { get; set; }

	}
}
