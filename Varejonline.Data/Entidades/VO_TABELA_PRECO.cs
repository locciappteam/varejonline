﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_TABELA_PRECO : VO_MODIFICACOES
    {
        public VO_TABELA_PRECO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        public string COD_TABELA { get; set; }

        public bool ATIVADO { get; set; }
        public int COD_TIPO_TABELA { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<VO_TABELA_PRECO_ITEM> VO_TABELA_PRECO_ITEMs { get; set; }
        public virtual ICollection<VO_TABELA_PRECO_RETORNO> VO_TABELA_PRECO_RETORNOs { get; set; }
        public virtual ICollection<VO_CLIENTE> VO_CLIENTEs { get; set; }
    }
}
