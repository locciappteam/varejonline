﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_CAMPANHA_RETORNO : VO_MODIFICACOES
    {
        public VO_CAMPANHA_RETORNO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("VO_CAMPANHA")]
        public int COD_CAMPANHA { get; set; }

        [ForeignKey("VO_CLIENTE")]
        public string CODIGO_SAP { get; set; }
        public string RETORNO_SUCESSO { get; set; }
        public string RETORNO_FALHO { get; set; }
        public string DESC_ERRO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_CAMPANHA VO_CAMPANHA { get; set; }
        public virtual VO_CLIENTE VO_CLIENTE { get; set; }
    }
}
