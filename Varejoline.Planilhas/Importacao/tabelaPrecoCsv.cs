﻿using DocumentFormat.OpenXml.Office.CustomUI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.Data.ViewModel;

namespace Varejonline.Planilhas.Importacao
{
    public class tabelaPrecoCsv
    {
        public static void Importar(string pathArquivo, string arquivoCsv)
        {
            int ErrosImportacao = 0;

            List<vmTabelaPreco> TabelaPrecoCSV = new List<vmTabelaPreco>();
            List<VO_IMPORTACAO_LOG> ListaLogImportacao;


            if (File.Exists(pathArquivo + arquivoCsv))
            {
                DateTime InicioImportacao = DateTime.Now;
                ListaLogImportacao = new List<VO_IMPORTACAO_LOG>();

                string novoNomeArquivo = String.Format($"{Path.GetFileNameWithoutExtension(arquivoCsv)}_{InicioImportacao.ToString("dd.MM.yyyy_hh.mm")}.csv");
                VO_IMPORTACAO_LOG voImportacaoLog;

                Log.Write(String.Format($"{DateTime.Now} :: Importando Planilha CSV - {pathArquivo}"));
                foreach (string line in File.ReadLines(pathArquivo + arquivoCsv, Encoding.GetEncoding("UTF-8")).Skip(1))
                {
                    try       
                    {
                        vmTabelaPreco _vmTabelaPreco = new vmTabelaPreco
                        {
                            COD_TABELA = (
                                line.Split(';')[0] == "20" ? String.Format($"{line.Split(';')[3]}-FRANQUIAS") 
                                :line.Split(';')[0] == "70" ? String.Format($"{line.Split(';')[3]}-70")
                                :line.Split(';')[0]
                            ),

                            COD_TIPO_TABELA = 
                            (
                                line.Split(';')[0] == "20" ? 20

                                :line.Split(';')[0] == "30" ? 30
                                  :line.Split(';')[0].Contains("ATC") ? 30

                                :line.Split(';')[0] == "30" ? 30
                                  :line.Split(';')[0].Contains("ATACADO") ? 30

                                :line.Split(';')[0] == "70" ? 70
                                 :line.Split(';')[0].Contains("SPA") ? 70
                             
                                :line.Split(';')[0] == "80" ? 80
                                  :line.Split(';')[0].Contains("B2B") ? 80

                              : 0
                            ), 
                            ATIVADO = true,
                            COD_PRODUTO = line.Split(';')[1],
                            VALOR_UNITARIO = Convert.ToDecimal(line.Split(';')[2].Replace(".", ","), new CultureInfo("pt-BR")),
                            PRODUTO_ATIVADO = true,
                            UF = line.Split(';')[3]
                        };

                        TabelaPrecoCSV.Add(_vmTabelaPreco);

                    }
                    catch (Exception Ex)
                    {
                        ErrosImportacao++;
                        Log.Write(String.Format($"{DateTime.Now} : Erro ao importar CSV: [{line.Split(';')[3]} - {line.Split(';')[1]}] ADVERTÊNCIA(S) [{Ex.Message}]"));
                        continue;
                    }
                }

                List<VO_TABELA_PRECO> VoTabelaPreco;
                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    VoTabelaPreco = (
                        from
                            voTabelaPreco in TabelaPrecoCSV.Select(a => new { a.COD_TABELA, a.COD_TIPO_TABELA, a.UF, a.ATIVADO }).Distinct()
                            //voTabelaPreco in TabelaPrecoCSV.Select(a => new { a.COD_TABELA, a.UF, a.ATIVADO, a.COD_TIPO_TABELA }).Distinct()
                        select new VO_TABELA_PRECO
                        {
                            COD_TABELA = voTabelaPreco.COD_TABELA,
                            ATIVADO = true,
                            COD_TIPO_TABELA = voTabelaPreco.COD_TIPO_TABELA,

                            VO_TABELA_PRECO_ITEMs = (
                                from
                                    a in TabelaPrecoCSV
                                join 
                                    b in _ctx.voProduto on a.COD_PRODUTO equals b.COD_PRODUTO
                                join
                                    c in _ctx.voTabelaPrecoItem on new { a.COD_TABELA, a.COD_PRODUTO } equals new { c.COD_TABELA, c.COD_PRODUTO } into _c
                                from 
                                    _C in _c.DefaultIfEmpty()
                                where
                                    voTabelaPreco.COD_TABELA == a.COD_TABELA
                                    && a.VALOR_UNITARIO != _C?.VALOR_UNITARIO
                                group
                                    a by new
                                    {
                                        COD_TABELA = a.COD_TABELA,
                                        COD_PRODUTO = a.COD_PRODUTO,
                                    }
                                into
                                    GB
                                let
                                    MAX_VALOR_UNITARIO = GB.Max(a => a.VALOR_UNITARIO)
                                select new VO_TABELA_PRECO_ITEM
                                {
                                    COD_TABELA = GB.Key.COD_TABELA,
                                    COD_PRODUTO = GB.Key.COD_PRODUTO,
                                    VALOR_UNITARIO = MAX_VALOR_UNITARIO,
                                    ATIVO = true
                                }
                            ).ToList()
                        }
                    ).Distinct().ToList();  
                 }

                voImportacaoLog = new VO_IMPORTACAO_LOG();

                foreach (VO_TABELA_PRECO TabelaPrecoCapa in VoTabelaPreco)
                {

                    voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                    voImportacaoLog.NOME_ARQUIVO = novoNomeArquivo;
                    voImportacaoLog.TIPO_IMPORTACAO = "TABELA_PRECO";
                    voImportacaoLog.CODIGO_IDETIFICADOR = TabelaPrecoCapa.COD_TABELA;

                    Log.Write(String.Format($"{DateTime.Now} : Importando Tabela de Preço: [{TabelaPrecoCapa.COD_TABELA}]"));
                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        try
                        {
                            if (_ctx.voTabelaPreco.Where(a => a.COD_TABELA == TabelaPrecoCapa.COD_TABELA).Count() == 0)
                            {
                                TabelaPrecoCapa.DATA_CADASTRO = DateTime.Now;
                                _ctx.Set<VO_TABELA_PRECO>().Add(TabelaPrecoCapa);
                                _ctx.SaveChanges();

                                voImportacaoLog.STATUS = "INSERIDO";
                            }

                            if(voImportacaoLog.STATUS != "INSERIDO")
                            {
                                voImportacaoLog.STATUS = "ATUALIZADO";
                            }

                            ListaLogImportacao.Add(voImportacaoLog);
                        }
                        catch(Exception ex)
                        {
                            voImportacaoLog.STATUS = "ERRO";
                            string erro = ex.Message;
                            // Traducao  de erro 

                            // -----------------------
                            voImportacaoLog.DESC_ERRO = erro;
                            ListaLogImportacao.Add(voImportacaoLog);
                            continue;
                        }
                    }


                    foreach (VO_TABELA_PRECO_ITEM TabelaPrecoItem in TabelaPrecoCapa.VO_TABELA_PRECO_ITEMs)
                    {

                        voImportacaoLog = null;
                        voImportacaoLog = new VO_IMPORTACAO_LOG();

                        voImportacaoLog.DATA_IMPORTACAO = InicioImportacao;
                        voImportacaoLog.NOME_ARQUIVO = novoNomeArquivo;
                        voImportacaoLog.TIPO_IMPORTACAO = "TABELA_PRECO_ITEM";

  
                        voImportacaoLog.CODIGO_IDETIFICADOR = TabelaPrecoItem.COD_TABELA + "@" + TabelaPrecoItem.COD_PRODUTO;

                        try
                        {
                            TabelaPrecoItem.VO_TABELA_PRECO = null;
                            using (DataContextVarejonline _ctx = new DataContextVarejonline())
                            {
                                VO_TABELA_PRECO_ITEM VoTabelaPrecoItem = _ctx.voTabelaPrecoItem
                                    .AsNoTracking()
                                    .FirstOrDefault(a =>
                                         a.COD_TABELA == TabelaPrecoItem.COD_TABELA &&
                                         a.COD_PRODUTO == TabelaPrecoItem.COD_PRODUTO);

                                if (VoTabelaPrecoItem == null)
                                {
                                    _ctx.Set<VO_TABELA_PRECO_ITEM>().Add(TabelaPrecoItem);
                                    _ctx.SaveChanges();

                                    voImportacaoLog.STATUS = "INSERIDO";
                                    ListaLogImportacao.Add(voImportacaoLog);
                                }
                                else
                                {
                                    _ctx.Entry(TabelaPrecoItem).State = EntityState.Modified;
                                    _ctx.SaveChanges();

                                    voImportacaoLog.STATUS = "ATUALIZADO";
                                    ListaLogImportacao.Add(voImportacaoLog);

                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            ErrosImportacao++;
                            voImportacaoLog.STATUS = "ERRO";

                            // Traducao Erro 
                            string erro = Ex.Message;
                            voImportacaoLog.DESC_ERRO = erro;
                            // -----------

                            ListaLogImportacao.Add(voImportacaoLog);
                            Log.Write(String.Format($"{DateTime.Now} : ERRO [TABELA DE PREÇO: {TabelaPrecoCapa.COD_TABELA} | PRODUTO: {TabelaPrecoItem.COD_PRODUTO}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
                            continue;
                        }
                    }
                }

                /*
                if (ErrosImportacao > 0)
                {
                    Email.Enviar(
                            "am03svc-cegid@loccitane.com.br",
                            "dlbr-itapplications@loccitane.com",
                            "Erro ao importar a Planilha de Tabela de Preço. Verifique o LOG.",
                            "Erro ao importar Planilha - Tabela de Preço"
                    );
                }
                */

                foreach (var log in ListaLogImportacao)
                {
                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        _ctx.Set<VO_IMPORTACAO_LOG>().Add(log);
                        _ctx.SaveChanges();
                    }
                }

                //MOVE ARQUIVO PARA PASTA DE PROCESSADO ===============================================
                Log.Write(String.Format($"{DateTime.Now} :: Enviando para pasta Processado - Produto {arquivoCsv}"));
                File.Move(
                    pathArquivo + arquivoCsv,
                    String.Format($@"{pathArquivo}\Processado\{novoNomeArquivo}")
                );

                //RELATÓRIO DE IMPORTAÇÃO DE PLANILHA 

                List<string> VoEmailDestinatario = new DataContextVarejonline().voEmailDestinatario
                              .AsNoTracking()
                              .Where(a => a.REL_TABPRECO == true)
                              .Select(a => a.EMAIL)
                              .ToList();

                Email.Enviar(
                           "am03svc-cegid@loccitane.com.br", String.Join(",", VoEmailDestinatario),
                           MensagemHtmlLogTabelaPreco.Formatar(ListaLogImportacao),
                           String.Format($"Relatório Importação Planilha - Tabela de Preco {novoNomeArquivo}"),
                           true
                   );
            }
            else
            {
                Log.Write(String.Format($"{DateTime.Now} :: Planilha CSV - Não localizada {pathArquivo + arquivoCsv}"));
            }
        }
    }
}