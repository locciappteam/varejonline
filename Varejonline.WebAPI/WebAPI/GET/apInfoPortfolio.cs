﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.GET
{
    public class apInfoPortfolio
    {
        public static List<filtroPortfolioJson> Get(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut)
        {
            try
            {
                string responseData = "";
                Task.Run(async () =>
                {
                    responseData = await API.Get(authWebAPI, baseAddress, uriEndereco, timeOut);
                    //saveResponse(responseData);
                }).GetAwaiter().GetResult();

                return JsonConvert.DeserializeObject<List<filtroPortfolioJson>>(responseData);
            }
            catch (Exception Ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                return null;
            }
        }
    }
}
