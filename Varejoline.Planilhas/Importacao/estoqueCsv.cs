﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;

namespace Varejonline.Planilhas.Importacao
{
    public class estoqueCsv
    {
        public static void Importar(string pathArquivo, string arquivoCsv)
        {
            if (File.Exists(pathArquivo + arquivoCsv))
            {
                int ErrosImportacao = 0;
                int i = 0;

                Log.Write(String.Format($"{DateTime.Now} :: Importando Planilha CSV - {arquivoCsv}"));

                //EXCLUI ESTOQUE ==================================================================
                Log.Write(String.Format($"{DateTime.Now} : Excluindo Estoque"));

                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    List<VO_ESTOQUE_CONTROLE> VoEstoqueControle = _ctx.voEstoqueControle.ToList();

                    _ctx.Set<VO_ESTOQUE_CONTROLE>().RemoveRange(VoEstoqueControle);
                    _ctx.SaveChanges();
                }

                //IMPORTA CSV =====================================================================
                Log.Write(String.Format($"{DateTime.Now} : Importando itens"));

                List<VO_ESTOQUE_PRODUTO> VoEstoqueProdutos = new List<VO_ESTOQUE_PRODUTO>();
                foreach(string line in File.ReadLines(pathArquivo + arquivoCsv, Encoding.GetEncoding("iso-8859-1")).Skip(1))
                {
                    i++;

                    try
                    {
                        VO_ESTOQUE_PRODUTO VoEstoqueProduto = new VO_ESTOQUE_PRODUTO
                        {
                            COD_PRODUTO = line.Split(';')[0],
                            QUANTIDADE = Convert.ToInt32(line.Split(';')[1].Replace(".", ""))
                        };
                        VoEstoqueProdutos.Add(VoEstoqueProduto);
                    }
                    catch (Exception Ex)
                    {
                        ErrosImportacao++;

                        Log.Write(String.Format($"{DateTime.Now} : ERRO [LINHA: {i} | PRODUTO: {line.Split(';')[0]}] - ADVERTÊNCIA(S) [{Ex.Message}]"));
                        continue;
                    }
                }

                //ADICIONA ESTOQUE ================================================================
                Log.Write(String.Format($"{DateTime.Now} : Adicionando Estoque"));

                using (DataContextVarejonline _ctx = new DataContextVarejonline())
                {
                    VO_ESTOQUE_CONTROLE VoEstoqueControle = new VO_ESTOQUE_CONTROLE
                    {
                        VO_ESTOQUE_PRODUTOs = (
                            from
                                a in _ctx.voProduto.AsEnumerable()
                            from
                                b in VoEstoqueProdutos.Where(csv => csv.COD_PRODUTO == a.COD_PRODUTO).DefaultIfEmpty()
                            select new VO_ESTOQUE_PRODUTO
                            {
                                COD_PRODUTO = a.COD_PRODUTO,
                                QUANTIDADE = b?.QUANTIDADE ?? 0
                            }
                        ).Distinct().ToList()
                    };

                    _ctx.Set<VO_ESTOQUE_CONTROLE>().Add(VoEstoqueControle);
                    _ctx.SaveChanges();
                }

                if (ErrosImportacao > 0)
                {

                    List<string> VoEmailDestinatario = new DataContextVarejonline().voEmailDestinatario
                            .AsNoTracking()
                            .Where(a => a.REL_ESTOQUE == true)
                            .Select(a => a.EMAIL)
                            .ToList();

                    Email.Enviar(
                            "am03svc-cegid@loccitane.com.br", String.Join(",", VoEmailDestinatario),
                            "Erro ao importar a Planilha de Estoque. Verifique o LOG.",
                            "Erro ao importar Planilha - Estoque"
                    );
                }

                //MOVE ARQUIVO PARA PASTA DE PROCESSADO ===============================================
                Log.Write(String.Format($"{DateTime.Now} :: Enviando para pasta Processado - Estoque {arquivoCsv}"));
                File.Move(
                    pathArquivo + arquivoCsv,
                    String.Format($@"{pathArquivo}\Processado\{Path.GetFileNameWithoutExtension(arquivoCsv)}_{DateTime.Now.ToString("dd.MM.yyyy_hh.mm")}.csv")
                );
            }
            else
            {
                Log.Write(String.Format($"{DateTime.Now} :: Planilha CSV - Não localizada {pathArquivo + arquivoCsv}"));
            }
        }
    }
}
