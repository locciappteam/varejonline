﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Varejonline.Data.AtributoValidacao;

namespace Varejonline.Data.Entidades
{
    public class VO_CLIENTE : VO_MODIFICACOES
    {        
        public VO_CLIENTE()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string CODIGO_SAP { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string DOCUMENTO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(150, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string NOME { get; set; }

        [StringLength(20, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string APELIDO { get; set; }

        public string GRUPO_ECONOMICO { get; set; }

        [ForeignKey("VO_TABELA_PRECO")]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(15, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_TABELA { get; set; }

        [ForeignKey("VO_PORTFOLIO")]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_PORTFOLIO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public bool UTILIZA_FATOR_COMPRA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal DESCONTO_PADRAO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(50, ErrorMessage = "{0} superior a {1} caracteres.")]
        [EmailAddress(ErrorMessage = "O campo {0} não é um E-mail válido")]
        public string EMAIL { get; set; }

        public decimal? PERCENTUAL_PARTICIPACAO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public bool FRANQUIA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public bool ATIVO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public int CANAL { get; set; }

        //[RequiredIf("FRANQUIA", true, "O campo MARCA obrigatório quando FRANQUIA")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string MARCA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(2, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string UF { get; set; }

        public decimal? VALOR_MINIMO_PEDIDO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_TABELA_PRECO VO_TABELA_PRECO { get; set; }
        public virtual VO_PORTFOLIO VO_PORTFOLIO { get; set; }
        public virtual VO_CLIENTE_ENDERECO VO_CLIENTE_ENDERECO { get; set; }
        public virtual VO_CLIENTE_TELEFONE VO_CLIENTE_TELEFONE { get; set; }
        public virtual VO_CLIENTE_CELULAR VO_CLIENTE_CELULAR { get; set; }
        public virtual ICollection<VO_CLIENTE_RETORNO> VO_CLIENTE_RETORNOs { get; set; }
        public virtual ICollection<VO_CAMPANHA_RETORNO> VO_CAMPANHA_RETORNOs { get; set; }
        public virtual ICollection<VO_PEDIDO_CAPA> VO_PEDIDO_CAPAs { get; set; }
        public virtual ICollection<VO_CAMPANHA_CLIENTE> VO_CAMPANHA_CLIENTEs { get; set; }

    }
}
