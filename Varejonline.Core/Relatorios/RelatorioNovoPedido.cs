﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;

namespace Varejonline.Core.Relatorios
{
    public class RelatorioNovoPedido
    {
        private static DataContextVarejonline _ctx = new DataContextVarejonline();

        public static void Enviar()
        {

            //FRANQUIA
            EnviarEmail("FRQ");
            
            //ATACADO
            EnviarEmail("ATC");
            
            //SPA
            EnviarEmail("SPA");
            
            //B2B
            EnviarEmail("B2B");

            //WEB
            EnviarEmail("WEB");

            //HARUS
            EnviarEmail("HARUS");

        }

        private static void EnviarEmail(string canalDescricao)
        {
            List<int> canais = _ctx.voCanal.AsNoTracking().Where(p=> p.ABREVIACAO.Contains(canalDescricao)).Select(p=> p.COD_CANAL).ToList();
            
            List<VO_PEDIDO_CAPA> VoPedidoCapa =
                    _ctx.voPedidoCapa
                    .AsNoTracking()
                    .Where(a =>
                        a.PEDIDO_GERADO == true &&
                        a.EMAIL_ENVIADO == false &&
                        canais.Contains(a.VO_CLIENTE.CANAL)
                    ).ToList();

            List<string> VoEmailDestinatario =
                    _ctx.voEmailDestinatario
                    .AsNoTracking()
                    .Where(a => (
                        canalDescricao == "FRQ" ? a.FRANQUIA :
                        canalDescricao == "ATC" || canalDescricao == "WEB"? a.ATACADO :
                        canalDescricao  == "SPA" ? a.SPA :
                        canalDescricao == "B2B" || canalDescricao.Contains("HARUS") ? a.B2B :
                        false) == true
                    )
                    .Select(a => a.EMAIL)
                    .ToList();

            canalDescricao = canalDescricao   == "FRQ"    ? "FRANQUIAS"
                             : canalDescricao == "ATC" || canalDescricao == "WEB" ? "WEB"
                             : canalDescricao == "B2B" ? "B2B"
                             : canalDescricao.Contains("HARUS") ? "HARUS" 
                             : canalDescricao == "SPA"  ? "SPA":"";
 
            if (VoPedidoCapa.Any() && VoEmailDestinatario.Any())
            {
                Log.Write(String.Format($"{DateTime.Now} : E-MAIL NOVO PEDIDO"));
                Log.Write(String.Format($"{DateTime.Now} :: CANAL: [{canalDescricao}] PEDIDOS: [{String.Join(", ", VoPedidoCapa.Select(a => a.NUMERO))}]"));

                string Mensagem = MensagemHTML.Formatar(VoPedidoCapa);
                Email.Enviar(
                    "am03svc-cegid@loccitane.com.br",
                    String.Join(",", VoEmailDestinatario),
                    Mensagem,
                    String.Format($"[Portal B2B] - Novo Pedido {canalDescricao}"),
                    true
                );

                foreach (var Pedido in VoPedidoCapa)
                {
                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        Pedido.EMAIL_ENVIADO = true;

                        _ctx.Entry<VO_PEDIDO_CAPA>(Pedido).State = EntityState.Modified;
                        _ctx.SaveChanges();
                    }
                }
            }
        }
    }
}
