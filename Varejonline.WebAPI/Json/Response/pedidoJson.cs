﻿using System.Collections.Generic;
using System.Linq;
using Varejonline.Data.DataContext;

namespace Varejonline.WebAPI.Json.Response
{
    public class pedidoJson
    {
        public string codigoSap { get; set; }
        public string numero { get; set; }
        public string documentoCliente { get; set; }
        public string dataCompra { get; set; }
        public decimal valorTotal { get; set; }
        public decimal valorDesconto { get; set; }
        public decimal valorLiquido { get; set; }
        public string campanhaVigente { get; set; }
        public string type { get; set; }
        public string observacao { get; set; }
        public string canal { get; set; }
        public List<PedidosItem> itens { get; set; }

        /*public void BuscarCodigoSAP()
        {
            if(canal == "GERAL")
            {
                codigoSap = new DataContextVarejonline().voCliente.Where(p => p.DOCUMENTO == documentoCliente).FirstOrDefault().CODIGO_SAP;
            }
            else
            {
                codigoSap = new DataContextVarejonline().voCliente.Where(p => p.DOCUMENTO == documentoCliente && p.CANAL.ToString() == canal).FirstOrDefault().CODIGO_SAP;
            }
        }
        */
    }

    public class PedidosItem
    {
        public string codigoSistema { get; set; }
        public int quantidade { get; set; }
        public decimal valorUnitario { get; set; }
        public decimal valorTotal { get; set; }
        public decimal valorDesconto { get; set; }
        public decimal valorLiquido { get; set; }
    }
}
