﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using Varejonline.Data.Entidades;

namespace Varejonline.Data.DataContext
{
    public class DataContextVarejonline : DbContext
    {
        public DataContextVarejonline()
        :base(@"Data Source=AMSAOCBRDB01;Initial Catalog=PORTAL_LOCCITANE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        //:base(@"Data Source=AMSAOTSQL01\CEGID;Initial Catalog=VAREJONLINE;Persist Security Info=True;User ID=portalloccitane;Password=3u3NO483")
        {
            this.Database.CommandTimeout = 300;
        }

        public DbSet<VO_PORTFOLIO> voPortfolio { get; set; }
        public DbSet<VO_PORTFOLIO_PRODUTO> voPortfolioProduto { get; set; }
        public DbSet<VO_PORTFOLIO_RETORNO> voPortfolioRetorno { get; set; }

        public DbSet<VO_CAMPANHA> voCampanha { get; set; }
        public DbSet<VO_CAMPANHA_CLIENTE> voCampanhaCliente { get; set; }
        public DbSet<VO_CAMPANHA_ATIVA> voCampanhaAtiva { get; set; }
        public DbSet<VO_CAMPANHA_RETORNO> voCampanhaRetorno { get; set; }

        public DbSet<VO_CANAL> voCanal { get; set; }

        public DbSet<VO_PEDIDO_CAPA> voPedidoCapa { get; set; }
        public DbSet<VO_PEDIDO_ITEM> voPedidoItem { get; set; }
        public DbSet<VO_PEDIDO_ARQUIVO> voPedioArquivo { get; set; }
        public DbSet<VO_PEDIDO_RETORNO> voPedidoRetorno { get; set; }

        public DbSet<VO_TABELA_PRECO> voTabelaPreco { get; set; }
        public DbSet<VO_TABELA_PRECO_ITEM> voTabelaPrecoItem { get; set; }
        public DbSet<VO_TABELA_PRECO_RETORNO> voTabelaPrecoRetorno { get; set; }

        public DbSet<VO_CLIENTE> voCliente { get; set; }
        public DbSet<VO_CLIENTE_ENDERECO> voClienteEndereco { get; set; }
        public DbSet<VO_CLIENTE_TELEFONE> voClienteTelefone { get; set; }
        public DbSet<VO_CLIENTE_CELULAR> voClienteCelular { get; set; }
        public DbSet<VO_CLIENTE_RETORNO> voClienteRetorno { get; set; }

        public DbSet<VO_PRODUTO> voProduto { get; set; }
        public DbSet<VO_PRODUTO_CATEGORIA> voProdutoCategoria { get; set; }
        public DbSet<VO_PRODUTO_GRADE> voProdutoGrade { get; set; }
        public DbSet<VO_PRODUTO_RETORNO> voProdutoRetorno { get; set; }

        public DbSet<VO_ESTOQUE_CONTROLE> voEstoqueControle { get; set; }
        public DbSet<VO_ESTOQUE_PRODUTO> voEstoqueProduto { get; set; }
        public DbSet<VO_ESTOQUE_RETORNO> voEstoqueRetorno { get; set; }

        public DbSet<VO_EMAIL_DESTINATARIO> voEmailDestinatario { get; set; }

        public DbSet<VO_PARAMETROS> voParametros { get; set; }

        public DbSet<VO_IMPORTACAO_LOG> voImportacaoLog { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public override int SaveChanges()
        {
            var context = ((IObjectContextAdapter)this).ObjectContext;
            IEnumerable<ObjectStateEntry> objectStateEntries =
                from e in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified)
                where
                    e.IsRelationship == false &&
                    e.Entity != null &&
                    typeof(VO_MODIFICACOES).IsAssignableFrom(e.Entity.GetType())
                select e;

            foreach (var entry in objectStateEntries)
            {
                dynamic entityBase = entry.Entity;

                if (entry.State == EntityState.Added || entityBase.DATA_CADASTRO == DateTime.MinValue)
                {
                    entityBase.DATA_CADASTRO = DateTime.Now;
                }
                entityBase.DATA_ALTERACAO = DateTime.Now;
            }



            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException DbEx)
            {
                var MensagensErro = DbEx.EntityValidationErrors
                        .SelectMany(a => a.ValidationErrors)
                        .Select(a => a.ErrorMessage);

                throw new DbEntityValidationException(string.Join("; ", MensagensErro));
            }
            catch (DbUpdateException UpEx)
            {
                throw new DbUpdateException(UpEx?.InnerException?.InnerException?.Message);
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
    }
}
