﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Emails;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;

namespace Varejonline.Core.Relatorios
{
    public class RelatorioSemanal
    {
        private static DataContextVarejonline _ctx = new DataContextVarejonline();

        public static void Enviar()
        {
            DateTime DataInicio = DateTime.Today.AddDays(-7);
            DateTime DataFinal = DateTime.Today;
            /*
            List<int> canais = _ctx.voPortfolio.Select(p => p.COD_PORTFOLIO).Distinct().ToList();
            foreach (var canal in canais)
            {
                List<VO_PEDIDO_CAPA> VoPedidoCapa = _ctx.voPedidoCapa
                .AsNoTracking()
                .Where(a => a.DATA_COMPRA >= DataInicio && a.DATA_COMPRA <= DataFinal)
                .ToList();

                var lista = from pedido in _ctx.voPedidoCapa.AsNoTracking()
                                join cliente in _ctx.voCliente.AsNoTracking()
                                on pedido.CODIGO_SAP equals cliente.CODIGO_SAP
                                where cliente.CANAL == canal 
                                && pedido.DATA_COMPRA >= DataInicio && pedido.DATA_COMPRA <= DataFinal
                                select pedido;

                List<VO_PEDIDO_CAPA> novaLista = new List<VO_PEDIDO_CAPA>();
                foreach(VO_PEDIDO_CAPA item in lista)
                {
                    novaLista.Add(item);
                }
           
                string nomeCanal = "";

                List<string> VoEmailDestinatario = null;

                if (canal == 20)
                {
                    nomeCanal = "FRANQUIAS";

                    VoEmailDestinatario = _ctx.voEmailDestinatario
                   .AsNoTracking()
                   .Where(a => a.SEMANAL == true
                   && a.FRANQUIA == true
                   ).Select(a => a.EMAIL)
                   .ToList();
                }

                if (canal == 30)
                {
                    nomeCanal = "ATACADO";
                    VoEmailDestinatario = _ctx.voEmailDestinatario
                   .AsNoTracking()
                   .Where(a => a.SEMANAL == true
                   && a.ATACADO == true
                   ).Select(a => a.EMAIL)
                   .ToList();
                }

                if (canal == 70)
                {
                    nomeCanal = "SPA";
                    VoEmailDestinatario = _ctx.voEmailDestinatario
                   .AsNoTracking()
                   .Where(a => a.SEMANAL == true
                   && a.ATACADO == true
                   ).Select(a => a.EMAIL)
                   .ToList();
                }

                if (canal == 80)
                {
                    nomeCanal = "B2B";
                    VoEmailDestinatario = _ctx.voEmailDestinatario
                   .AsNoTracking()
                   .Where(a => a.SEMANAL == true
                   && a.B2B == true
                   ).Select(a => a.EMAIL)
                   .ToList();
                }


                if (novaLista.Any() && VoEmailDestinatario.Any())
                {
                    Log.Write(String.Format($"{DateTime.Now} : Enviando E-mail Relatório Consolidado da Semana"));

                    string Mensagem = MensagemHTML.Formatar(novaLista);
                    Email.Enviar(
                        "am03svc-cegid@loccitane.com.br",
                        String.Join(",", VoEmailDestinatario),
                        Mensagem,
                        "Portal de Pedidos - Relatório Consolidado da Semana (" + nomeCanal + ")",
                        true
                    );
                }
            
            }
            */
        }
    }
}
