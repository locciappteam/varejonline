﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_TABELA_PRECO_RETORNO : VO_MODIFICACOES
    {
        public VO_TABELA_PRECO_RETORNO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("VO_TABELA_PRECO")]
        public string COD_TABELA { get; set; }

        [ForeignKey("VO_PRODUTO")]
        public string COD_PRODUTO { get; set; }

        public string RETORNO_SUCESSO { get; set; }
        public string RETORNO_FALHO { get; set; }
        public string DESC_ERRO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_TABELA_PRECO VO_TABELA_PRECO { get; set; }
        public virtual VO_PRODUTO VO_PRODUTO { get; set; }
    }
}
