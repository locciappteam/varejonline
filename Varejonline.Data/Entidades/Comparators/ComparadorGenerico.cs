﻿using System;
using System.Collections.Generic;

namespace Varejonline.Data.Entidades.Comparators
{
    public class ComparadorGenerico<T> : IEqualityComparer<T>
    {
        public Func<T, T, bool> MetodoEquals { get; }
        public Func<T, int> MetodoGetHashCode { get; }

        public ComparadorGenerico(
        Func<T, T, bool> metodoEquals,
        Func<T, int> metodoGetHashCode)
        {
            this.MetodoEquals = metodoEquals;
            this.MetodoGetHashCode = metodoGetHashCode;
        }

        public bool Equals(T x, T y)
        {
            return MetodoEquals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return MetodoGetHashCode(obj);
        }

    }
}
