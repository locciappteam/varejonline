﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_PRODUTO : VO_MODIFICACOES
    {
        public VO_PRODUTO()
        {
            URL_FOTO = "";
            ATIVADO = true;
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [StringLength(15, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_PRODUTO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(15, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_BARRA { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(100, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string NOME { get; set; }

        public string UNID_PRINCIPAL { get; set; }

        [StringLength(15, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string NCM { get; set; }
        public string URL_FOTO { get; set; }
        public bool ATIVADO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<VO_ESTOQUE_PRODUTO> VO_ESTOQUE_PRODUTOs { get; set; }
        public virtual ICollection<VO_PEDIDO_ITEM> VO_PEDIDO_ITEMs { get; set; }
        public virtual ICollection<VO_TABELA_PRECO_ITEM> VO_TABELA_PRECO_ITEMs { get; set; }
        public virtual VO_PRODUTO_CATEGORIA VO_PRODUTO_CATEGORIA { get; set; }
        public virtual ICollection<VO_PRODUTO_GRADE> VO_PRODUTO_GRADEs { get; set; }
        public virtual ICollection<VO_PRODUTO_RETORNO> VO_PRODUTO_RETORNOs { get; set; }
        public virtual ICollection<VO_ESTOQUE_RETORNO> VO_ESTOQUE_RETORNOs { get; set; }
        public virtual ICollection<VO_PORTFOLIO_RETORNO> VO_PORTFOLIO_RETORNOs { get; set; }
    }
}
