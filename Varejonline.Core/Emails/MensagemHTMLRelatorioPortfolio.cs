﻿using System;
using System.Collections.Generic;
using System.Linq;
using Varejonline.Data.ViewModel;

namespace Varejonline.Core.Emails
{
    public class MensagemHTMLRelatorioPortfolio
    {
        public static void Formatar(List<vmRelatorioPortfolio> vmRelatorioPortfolio)
        {

            var arquivos = from a in vmRelatorioPortfolio
                           group a by a.NOME_ARQUIVO into newGroup
                           select newGroup;

            foreach (var arquivo in arquivos)
            {
                               
                string Mensagem = "<style>" +
                        "table.blueTable{border:0px solid;width:100%;text-align:left;border-collapse:collapse;}" +

                        "table.blueTable td, table.blueTable th{border:1px solid #AAAAAA;}" +
                        "table.blueTable tbody td{font-size:13px;}" +
                        "table.blueTable tr:nth-child(even){background:#D0E4F5;}" +

                        "table.blueTable thead{background:#85e085;}" +
                        "table.blueTable thead th{font-size:15px;font-weight:bold;color:#FFFFFF;border-left:2px solid #D0E4F5;}" +
                        "table.blueTable thead th:first-child{border-left:none;}" +

                        "table.blueTable tfoot{font-size:14px;font-weight:bold;color:#FFFFFF;background:#D0E4F5;background:-moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:-webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);background:linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%);border-top:2px solid #444444;}" +
                        "table.blueTable tfoot td{font-size:14px;}" +
                        "table.blueTable tfoot .links{text-align:right;}" +
                        "table.blueTable tfoot .links a{display:inline-block;background:#1C6EA4;color:#FFFFFF;padding:2px 8px;border-radius:5px;}" +

                        "table.blueTable caption{ color: white; background:gray;}" +

                    "</style>";


                Mensagem +=
                "<table class='blueTable'>" +
                "<caption> INFORMAÇÕES DA IMPORTAÇÃO COMPLETA DE PORTFÓLIO (ENVIO E RETORNO) </caption>" +
                       "<thead>" +
                        string.Format($"<tr><td>NOME ARQUIVO</td><td> {arquivo.Select(a => a.NOME_ARQUIVO).FirstOrDefault()} </td></tr>") +
                       string.Format($"<tr><td>DATA IMPORTAÇÃO</td><td> {arquivo.Select(a => a.DATA_IMPORTACAO).FirstOrDefault()} </td></tr>") +
                   "</thead>" +
               "</table><br><br>";

                Mensagem +=
                "<table class='blueTable'>" +
                "<caption> PORTFOLIO </caption>" +
                "<thead>" +
                        "<tr>" +
                            "<th>COD. PORTFOLIO</th>" +
                            "<th>COD. PRODUTO</th>" +
                            "<th>STATUS IMPORTAÇÃO</th>" +
                            "<th>DESC. ERRO</th>" +
                            "<th>STATUS VAREJONLINE</th>" +
                        "</tr>" +
                    "</thead>" +
                    "<tbody>";

                foreach (vmRelatorioPortfolio log in arquivo.OrderBy(p=> p.STATUS_IMPORTACAO))
                {

                    if (log.STATUS_IMPORTACAO == "ERRO" || log.STATUS_VAREJONLINE == "Sem Retorno")
                    {
                        Mensagem +=
                        "<tr bgcolor= 'red' <font color='white'> " +
                            String.Format($"<td>{log.COD_PORTFOLIO}</td>") +
                            String.Format($"<td>{log.COD_PRODUTO}</td>") +
                            String.Format($"<td>{log.STATUS_IMPORTACAO}</td>") +
                            String.Format($"<td>{log.DESC_ERRO}</td>") +
                            String.Format($"<td>{log.STATUS_VAREJONLINE}</td>") +
                        "</font></tr>";
                    }
                    else
                    {
                        Mensagem +=
                        "<tr>" +
                            String.Format($"<td>{log.COD_PORTFOLIO}</td>") +
                            String.Format($"<td>{log.COD_PRODUTO}</td>") +
                            String.Format($"<td>{log.STATUS_IMPORTACAO}</td>") +
                            String.Format($"<td>{log.DESC_ERRO}</td>") +
                            String.Format($"<td>{log.STATUS_VAREJONLINE}</td>") +
                        "</tr>";
                    }

                }

                Email.Enviar(
                    "am03svc-cegid@loccitane.com.br",
                    "wagner.damasio@loccitane.com, leonardo.zapella@loccitane.com, renato.vidotto@loccitane.com",//"dlbr-itapplications@loccitane.com",
                    Mensagem,
                    String.Format($"Relatório Importação Planilha Completo - Portfólio"),
                    true
                );

            }
        }
    }
}
