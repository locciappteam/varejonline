﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_CAMPANHA_CLIENTE : VO_MODIFICACOES
    {
        public VO_CAMPANHA_CLIENTE()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, Column(Order = 0), ForeignKey("VO_CAMPANHA")]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public int COD_CAMPANHA { get; set; }

        [Key, Column(Order = 1), ForeignKey("VO_CLIENTE")]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string CODIGO_SAP { get; set; }

        [Key, Column(Order = 2)]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public DateTime DATA_INICIO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public DateTime DATA_FINAL { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_CAMPANHA VO_CAMPANHA { get; set; }
        public virtual VO_CLIENTE VO_CLIENTE { get; set; }
    }
}
