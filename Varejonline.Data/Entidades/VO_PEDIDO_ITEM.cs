﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Varejonline.Data.Entidades
{
    public class VO_PEDIDO_ITEM : VO_MODIFICACOES
    {
        public VO_PEDIDO_ITEM()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, Column(Order = 0), ForeignKey("VO_PEDIDO_CAPA")]
        public string NUMERO { get; set; }

        [Key, Column(Order = 1)]
        public int NUM_ITEM { get; set; }

        [ForeignKey("VO_PRODUTO")]
        public string COD_PRODUTO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public int QUANTIDADE { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_UNITARIO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_TOTAL { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_DESCONTO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        public decimal VALOR_LIQUIDO { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_PEDIDO_CAPA VO_PEDIDO_CAPA { get; set; }
        public virtual VO_PRODUTO VO_PRODUTO { get; set; }
    }
}
