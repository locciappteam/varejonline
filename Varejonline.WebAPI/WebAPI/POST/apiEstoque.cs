﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Request;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiEstoque
    {
        private static List<estoqueJson> getEstoqueJson(DateTime DataTransferencia)
        {
            List<estoqueJson> estoque;
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                estoque = (
                    from
                        EstoqueProduto in _ctx.voEstoqueProduto
                    where
                        EstoqueProduto.DATA_ALTERACAO >= DataTransferencia
                    select new estoqueJson
                    {
                        codigoSistema = EstoqueProduto.COD_PRODUTO,
                        quantidade = EstoqueProduto.QUANTIDADE
                    }
                ).ToList();
            }

            return estoque;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia)
        {
            List<estoqueJson> EstoqueJson = getEstoqueJson(DataTransferencia);

            for (int i = 0; i < EstoqueJson.Count(); i += 100)
            {
                try
                {
                    string jsonRequest = JsonConvert.SerializeObject(EstoqueJson.Skip(i).Take(100));
                    string responseData;

                    Task.Run(async () =>
                    {
                        responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);
                        saveResponse(responseData);
                    }).GetAwaiter().GetResult();
                }
                catch (Exception Ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                    continue;
                }
            }
        }

        private static void saveResponse(string responseData)
        {
            Log.Write(String.Format($"{DateTime.Now} : Salvando Retorno WebAPI"));

            if (responseData != "Falho")
            {
                responseJson ReponseJson = JsonConvert.DeserializeObject<responseJson>(responseData);
                string[] DescErro = ReponseJson.errorDescription.Split('|').Skip(1).ToArray();

                List<VO_ESTOQUE_RETORNO> VoEstoqueRetorno = new List<VO_ESTOQUE_RETORNO>();
                foreach (var retornosuccessProcessed in ReponseJson.successProcessed)
                {
                    VoEstoqueRetorno.Add(new VO_ESTOQUE_RETORNO
                    {
                        COD_PRODUTO = retornosuccessProcessed,
                        RETORNO_SUCESSO = "Retorno com sucesso!",
                        RETORNO_FALHO = null,
                        DESC_ERRO = null
                    });
                }

                foreach (var retornofailedProcessed in ReponseJson.failedProcessed)
                {
                    VoEstoqueRetorno.Add(new VO_ESTOQUE_RETORNO
                    {
                        COD_PRODUTO = retornofailedProcessed,
                        RETORNO_SUCESSO = null,
                        RETORNO_FALHO = "Retorno com erro!",
                        DESC_ERRO = DescErro[ReponseJson.failedProcessed.IndexOf(retornofailedProcessed)]
                    });
                }

                foreach (var retorno in VoEstoqueRetorno.Distinct())
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_ESTOQUE_RETORNO>().Add(retorno);
                            _ctx.SaveChanges();
                        }
                    }
                    catch (Exception Ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO SALVAR RETORNO [{retorno}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }
    }
}
