﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Varejonline.Core.Logs;

namespace Varejonline.WebAPI.WebAPI
{
    public class API
    {
        public static async Task<string> Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, string jsonRequest)
        {
            Log.Write(String.Format($"{DateTime.Now} : Chamada WebAPI [{baseAddress}{uriEndereco}]"));

            using (var httpClient = new HttpClient { BaseAddress = baseAddress })
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authWebAPI);

                httpClient.Timeout = TimeSpan.FromMinutes(timeOut);

                using (var content = new StringContent(jsonRequest, Encoding.Default, "application/json"))
                {
                    using (var response = await httpClient.PostAsync(uriEndereco, content))
                    {
                        string responseData = await response.Content.ReadAsStringAsync();

                        if (response.IsSuccessStatusCode)
                        {        
                            return responseData;
                        }
                        else
                        {
                            Log.Write(String.Format($"{DateTime.Now} : Erro retorno WebAPI {uriEndereco} [{responseData}]"));
                            return "Falho";
                        }
                    }
                }
            }
        }

        public static async Task<string> Get(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut)
        {
            Log.Write(String.Format($"{DateTime.Now} : Chamada WebAPI [{baseAddress}{uriEndereco}]"));

            using (var httpCliente = new HttpClient { BaseAddress = baseAddress })
            {
                httpCliente.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authWebAPI);

                httpCliente.Timeout = TimeSpan.FromMinutes(timeOut);

                using (var response = await httpCliente.GetAsync(uriEndereco))
                {
                    string responseData = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        return responseData;
                    }
                    else
                    {
                        Log.Write(String.Format($"{DateTime.Now} : Erro retorno WebAPI {uriEndereco} [{responseData}]"));
                        return "Falho";
                    }
                }
            }
        }
    }    
}
