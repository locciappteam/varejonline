﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Core.Relatorios;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.GET
{
    public class apiPedidos
    {
        public static void Get(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut)
        {
            try
            {
                string responseData;

                Task.Run(async () =>        
                {
                     responseData = await API.Get(authWebAPI, baseAddress, uriEndereco, timeOut);
               
                    saveResponse(responseData);
                }).GetAwaiter().GetResult();
            }
            catch (Exception Ex)
            {
                Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
            }
        }

        private static void saveResponse(string responseData)
        {
            if (responseData != "Falho")
            {
                List<pedidoJson> ReponseJson = JsonConvert.DeserializeObject<List<pedidoJson>>(responseData);
                List<string> pedidosNum = new DataContextVarejonline().voPedidoCapa.AsNoTracking().Where(p => p.EMAIL_ENVIADO == true && p.PEDIDO_GERADO == true).Select(p => p.NUMERO).ToList();

                var newResponse = ReponseJson.Where(p => !pedidosNum.Contains(p.numero)).ToList();

                foreach (var pedido in newResponse)
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {

                            VO_PEDIDO_CAPA VoPedidoCapa = new VO_PEDIDO_CAPA
                            {
                                NUMERO = pedido.numero,
                                CODIGO_SAP = pedido.codigoSap,
                                DATA_COMPRA = new DateTime(
                                                Convert.ToInt32(pedido.dataCompra.Substring(0, 10).Split('-')[2]),
                                                Convert.ToInt32(pedido.dataCompra.Substring(0, 10).Split('-')[1]),
                                                Convert.ToInt32(pedido.dataCompra.Substring(0, 10).Split('-')[0])
                                              ),
                                VALOR_TOTAL = pedido.valorTotal,
                                VALOR_DESCONTO = pedido.valorDesconto,
                                VALOR_LIQUIDO = pedido.valorLiquido,

                                PEDIDO_GERADO = false,
                                CAMPANHA_VINGENTE = pedido.campanhaVigente,
                                TIPO_COMPRA = pedido.type,
                                EMAIL_ENVIADO = false,
                                STATUS = null,
                                OBSERVACAO = pedido.observacao,

                                VO_PEDIDO_ITEMs = new List<VO_PEDIDO_ITEM>()
                            };
                            


                            if(pedido.canal == "GERAL")
                            {
                                pedido.codigoSap = _ctx.voCliente.Where(p => p.DOCUMENTO == pedido.documentoCliente).FirstOrDefault().CODIGO_SAP;
                            }
                            else // BUSCA POR CANAL ESPECIFICO INDICADO NO JSON 
                            {
                                try
                                {
                                    pedido.codigoSap = _ctx.voCliente.Where(p => p.DOCUMENTO == pedido.documentoCliente && p.CANAL.ToString() == pedido.canal.Substring(0, 2)).FirstOrDefault().CODIGO_SAP;
                                }
                                catch
                                {
                                    throw new Exception("CODIGO SAP DO CLIENTE NÃO ECONTRADO");
                                }
                                
                            }

                            if (pedido.codigoSap == null)
                            {
                                throw new Exception("CODIGO SAP DO CLIENTE NÃO ENCONTRADO");
                            }
                            else
                                VoPedidoCapa.CODIGO_SAP = pedido.codigoSap;

                            foreach (var item in pedido.itens)
                            {
                                VO_PEDIDO_ITEM VoPedidoItem = new VO_PEDIDO_ITEM
                                {
                                    NUMERO = pedido.numero,
                                    NUM_ITEM = pedido.itens.IndexOf(item) + 1,
                                    COD_PRODUTO = item.codigoSistema,
                                    QUANTIDADE = item.quantidade,
                                    VALOR_UNITARIO = item.valorUnitario,
                                    VALOR_TOTAL = item.valorTotal,
                                    VALOR_DESCONTO = item.valorDesconto,
                                    VALOR_LIQUIDO = item.valorLiquido
                                };

                                VoPedidoCapa.VO_PEDIDO_ITEMs.Add(VoPedidoItem);
                            }

                            VO_PEDIDO_CAPA regVoPedidoCapa = _ctx.voPedidoCapa.FirstOrDefault(a => a.NUMERO == VoPedidoCapa.NUMERO);
                            if (regVoPedidoCapa == null)
                            {
                                Log.Write(String.Format($"{DateTime.Now} : Obtendo Pedidos - Pedido {pedido.numero}"));

                                _ctx.Set<VO_PEDIDO_CAPA>().Add(VoPedidoCapa);
                                _ctx.SaveChanges();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO [{pedido.numero} | {pedido.documentoCliente}] ADVERTÊNCIA(s) [{ex.ToString()}]"));
                        continue;
                    }
                }

                RelatorioNovoPedido.Enviar();
            }

        }
    }
}
