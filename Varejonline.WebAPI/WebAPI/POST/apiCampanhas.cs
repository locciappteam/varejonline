﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Varejonline.Core.Logs;
using Varejonline.Data.DataContext;
using Varejonline.Data.Entidades;
using Varejonline.WebAPI.Json.Request;
using Varejonline.WebAPI.Json.Response;

namespace Varejonline.WebAPI.WebAPI.POST
{
    public class apiCampanhas
    {
        private static List<campanhaJson> getCampanhaJson(DateTime DataTransferencia)
        {
            List<campanhaJson> campanha;
            using (DataContextVarejonline _ctx = new DataContextVarejonline())
            {
                campanha = (
                    from
                        a  in _ctx.voCampanhaCliente
                    join b in _ctx.voCanal on a.VO_CAMPANHA.CANAL equals b.CANAL_SAP
                    where
                        a.DATA_ALTERACAO >= DataTransferencia
                    orderby
                        a.DATA_INICIO ascending
                    select new campanhaJson
                    {
                        campanha = a.VO_CAMPANHA.NOME_CAMPANHA,
                        inicio = a.DATA_INICIO,
                        fim = a.DATA_FINAL,
                        cliente = a.VO_CLIENTE.DOCUMENTO,
                        canal = b.COD_CANAL + "-" + b.ABREVIACAO
                    }
                ).ToList();
            }

            return campanha;
        }

        public static void Post(string authWebAPI, Uri baseAddress, string uriEndereco, int timeOut, DateTime DataTransferencia)
        {
            List<campanhaJson> CampanhaJson = getCampanhaJson(DataTransferencia).OrderBy(p=> p.inicio).ToList();

            for (int i = 0; i < CampanhaJson.Count(); i += 100)
            {
                try
                {
                    string jsonRequest = JsonConvert.SerializeObject(CampanhaJson.Skip(i).Take(100));
                    string responseData;

                    Task.Run(async () =>
                    {
                        responseData = await API.Post(authWebAPI, baseAddress, uriEndereco, timeOut, jsonRequest);
                      saveResponse(responseData);
                    }).GetAwaiter().GetResult();
                }
                catch (Exception Ex)
                {
                    Log.Write(String.Format($"{DateTime.Now} : ERRO REQUISIÇÃO WEB API [{uriEndereco}] - [{Ex.Message}] "));
                    continue;
                }
            }
        }

        private static void saveResponse(string responseData)
        {
            if (responseData != "Falho")
            {
                responseJson ReponseJson = JsonConvert.DeserializeObject<responseJson>(responseData);
                string[] DescErro = ReponseJson.errorDescription.Split('|').Skip(1).ToArray();

                List<VO_CAMPANHA_RETORNO> VoCampanhaRetorno = new List<VO_CAMPANHA_RETORNO>();

                int i = 0;
                foreach (var retornosuccessProcessed in ReponseJson.successProcessed)
                {
                    string NomeCampanha = ReponseJson.successProcessed[i].Substring(10, ReponseJson.successProcessed[i].IndexOf("Cliente:") - 11);
                    string Documento = ReponseJson.successProcessed[i].Substring(ReponseJson.successProcessed[i].IndexOf("Cliente:") + 9, 18);

                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    { 
                        VoCampanhaRetorno.Add(new VO_CAMPANHA_RETORNO
                        {
                            COD_CAMPANHA = _ctx.voCampanha.OrderByDescending(a => a.COD_CAMPANHA).FirstOrDefault(a => a.NOME_CAMPANHA == NomeCampanha).COD_CAMPANHA,
                            CODIGO_SAP = Documento,
                            RETORNO_SUCESSO = "Retorno com sucesso!",
                            RETORNO_FALHO = null,
                            DESC_ERRO = null
                        });
                    }

                    i++;
                }

                i = 0;
                foreach (var retornofailedProcessed in ReponseJson.failedProcessed)
                {
                    string NomeCampanha = ReponseJson.failedProcessed[i].Substring(10, ReponseJson.failedProcessed[i].IndexOf("Cliente:") - 11);
                    string Documento = ReponseJson.failedProcessed[i].Substring(ReponseJson.failedProcessed[i].IndexOf("Cliente:") + 9, 18);

                    using (DataContextVarejonline _ctx = new DataContextVarejonline())
                    {
                        VoCampanhaRetorno.Add(new VO_CAMPANHA_RETORNO
                        {
                            COD_CAMPANHA = _ctx.voCampanha.OrderByDescending(a => a.COD_CAMPANHA).FirstOrDefault(a => a.NOME_CAMPANHA == NomeCampanha).COD_CAMPANHA,
                            CODIGO_SAP = Documento,
                            RETORNO_SUCESSO = null,
                            RETORNO_FALHO = "Retorno com erro!",
                            DESC_ERRO = DescErro[ReponseJson.failedProcessed.IndexOf(retornofailedProcessed)]
                        });
                    }

                    i++;
                }

                foreach (var retorno in VoCampanhaRetorno.Distinct())
                {
                    try
                    {
                        using (DataContextVarejonline _ctx = new DataContextVarejonline())
                        {
                            _ctx.Set<VO_CAMPANHA_RETORNO>().Add(retorno);
                            _ctx.SaveChanges();
                        }
                    }
                    catch (Exception Ex)
                    {
                        Log.Write(String.Format($"{DateTime.Now} : ERRO SALVAR RETORNO [{retorno.COD_CAMPANHA}-{retorno.CODIGO_SAP}] - [{Ex.Message}] "));
                        continue;
                    }
                }
            }
        }
    }
}
