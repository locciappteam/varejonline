﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Varejonline.Data.Entidades
{
    public class VO_PEDIDO_ARQUIVO : VO_MODIFICACOES
    {
        public VO_PEDIDO_ARQUIVO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key, ForeignKey("VO_PEDIDO_CAPA")]
        public string NUMERO { get; set; }

        public string NOME_ARQUIVO { get; set; }
        public string LOCAL_ARQUIVO { get; set; }
        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual VO_PEDIDO_CAPA VO_PEDIDO_CAPA { get; set; }
    }
}
