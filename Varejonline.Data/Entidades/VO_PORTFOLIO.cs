﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Varejonline.Data.Entidades
{
    public class VO_PORTFOLIO : VO_MODIFICACOES
    {
        public VO_PORTFOLIO()
        {
            CADASTRADO_POR = "INTEGRACAO";
            DATA_CADASTRO = DateTime.Now;
            ALTERADO_POR = "INTEGRACAO";
            DATA_ALTERACAO = DateTime.Now;
        }

        [Key]
        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(25, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string COD_PORTFOLIO { get; set; }

        [Required(ErrorMessage = "O campo {0} não foi informado.")]
        [StringLength(50, ErrorMessage = "{0} superior a {1} caracteres.")]
        public string NOME { get; set; }

        public string MARCA { get; set; }

        public string CADASTRADO_POR { get; set; }
        public DateTime DATA_CADASTRO { get; set; }
        public string ALTERADO_POR { get; set; }
        public DateTime DATA_ALTERACAO { get; set; }

        public virtual ICollection<VO_PORTFOLIO_PRODUTO> VO_PORTFOLIO_PRODUTOs { get; set; }
        public virtual ICollection<VO_PORTFOLIO_RETORNO> VO_PORTFOLIO_RETORNOs { get; set; }
        public virtual ICollection<VO_CLIENTE> VO_CLIENTEs { get; set; }
    }
}
